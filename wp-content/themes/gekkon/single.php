<?php get_header(); ?>

	<section id="content">
		<section id="posts">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="breadc"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></div>
	
			<article class="post">
				<h1><?php $title = the_title( '', '', false ); if (get_field('h1')) {the_field('h1');} else {echo $title;} ?></h1>
				<div class="entry entry_pad">
					<?php the_content() ?>
					<div class="seo_text"><? the_field('seo_text');?></div>
					<div class="clear"></div>
				</div>
			</article>

			<?php $images_bot = get_post_meta(get_the_ID(), 'ar_images_bot', true); ?>
	
		<?php endwhile; endif; ?>
		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
		<?php include (TEMPLATEPATH . '/primeri.php'); ?>
	</section>

<?php get_footer(); ?>