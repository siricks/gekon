<?php   

if ( function_exists('register_sidebars') ) {
	register_sidebar(array('name'=>'sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title">',
		'after_title' => '</div>',
	));
}



if ( function_exists('register_nav_menus') ) {
	register_nav_menus(array(
		'Меню' => 'Главное меню',
		'МенюНиз' => 'Нижнее меню',
		'МенюЛево' => 'Левое меню',
		)
	);
}



remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );



include (TEMPLATEPATH . '/plugins/meta_boxes.php');



if ( function_exists( 'add_theme_support' ) ) {  
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'img260', 260, 260, true );
}
function raft_post_img_wp($rpiw_id, $rpiw_url=1, $rpiw_form=1, $rpiw_size='', $rpiw_bef='', $rpiw_aft='', $rpiw_where='', $rpiw_ta=1) {
	$raft_post_img_wp = '';
	if ($rpiw_size=='') { $rpiw_size='thumbnail'; }
	if($rpiw_where == '' || $rpiw_where == 0) {
		$rpiw_attimg = wp_get_attachment_image_src(get_post_thumbnail_id($rpiw_id), $rpiw_size);
		$rpiw_attimg_full = wp_get_attachment_image_src(get_post_thumbnail_id($rpiw_id), 'full');
	} else {
		$rpiw_attimg = wp_get_attachment_image_src($rpiw_where, $rpiw_size);
		$rpiw_attimg_full = wp_get_attachment_image_src($rpiw_where, 'full');
	}
	$rpiw_img = $rpiw_attimg[0];
	$rpiw_img_full = $rpiw_attimg_full[0];
	if($rpiw_img != '') {
		$rpiw_urll = '';
		$rpiw_urlr = '';
		if($rpiw_ta == 0) {
			$rpiw_title = '';
			$rpiw_alt = '';
		} elseif($rpiw_ta == 1) {
			$rpiw_title = ' title="' . get_the_title($rpiw_id) . '"';
			$rpiw_alt = ' alt="' . get_the_title($rpiw_id) . '"';
		} elseif($rpiw_ta == 2) {
			$rpiw_title = ' title="' . $rpiw_id->name . '"';
			$rpiw_alt = ' alt="' . $rpiw_id->name . '"';
		}
		$rpiw_image = '';
		if($rpiw_url == '1') {
			$rpiw_urll = '<a href="' . get_permalink($rpiw_id) . '"' . $rpiw_title . '>';
			$rpiw_urlr = '</a>';
		} elseif($rpiw_url == '2') {
			$rpiw_urll = '<a href="' . $rpiw_img_full . '"' . $rpiw_title . '>';
			$rpiw_urlr = '</a>';
		}
		if($rpiw_form == '0') {
			$rpiw_image = $rpiw_img;
		} elseif($rpiw_form == '1') {
			$rpiw_image = '<img src="' . $rpiw_img .'"' . $rpiw_title . '' . $rpiw_alt . ' />';
		} 
		$raft_post_img_wp = $rpiw_bef . $rpiw_urll . $rpiw_image . $rpiw_urlr . $rpiw_aft;
	}
	return $raft_post_img_wp;
}



function raft_announce($text, $announce_length = 120) {
	$text = preg_replace ("!\[/?.*\]!U", '', $text );
	$text=strip_tags($text);
	$string=$text;
	if ( mb_strlen($string,'UTF-8')>$announce_length )
	{
		$string = mb_substr($string, 0, $announce_length,'UTF-8');
		$position = mb_strrpos($string, ' ', 'UTF-8');
		$string = mb_substr($string, 0, $position, 'UTF-8').'...';
	}
	return $string;
}



function raft_custum_field($pid, $cfid, $cfform=0, $cfbefore='', $cfafter='') {
	$raft_custum_field = '';
	$raft_custum_field_entry = get_post_meta($pid, $cfid, true);
	if($cfform == 1) {
		$raft_custum_field_entry= trim(str_replace("\n", "<br>", $raft_custum_field_entry));
	} elseif($cfform == 2) {
		$raft_custum_field_entry = apply_filters('the_content', $raft_custum_field_entry);
	}
	if($raft_custum_field_entry != '') {
		$raft_custum_field = $cfbefore . $raft_custum_field_entry . $cfafter;
	}
	return $raft_custum_field;
}



function raft_comment($comment, $args, $depth) { $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<div class="comment_entry">
			<div class="comment_ava"><?php echo get_avatar( $comment, $size = '80' );  ?></div>
			<div class="comment_r">
				<div class="comment_ad">
					<span class="comment_a"><?php comment_author_link() ?></span>
					<span class="comment_d"><?php comment_date('d.m.Y') ?></span>
				</div>
				<div class="comment_text">
					<?php if ($comment->comment_approved == '0') : ?><p><em><?php _e('Ваш комментарий ожидает модерации.') ?></em></p><?php endif; ?>
					<?php comment_text() ?>
				</div>
				<div class="comment_repl"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></div>
			</div>
			<div class="clear"></div>
		</div>
<?php  }



if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
		foreach ( (array) $cats as $cat ) {
			$descendants = get_term_children( (int) $cat, 'category' );
			if ( $descendants && in_category( $descendants, $_post ) )
				return true;
		}
		return false;
	}
}



function my_custom_post_prod() {
	$labels = array(
		'name'               => _x( 'Товары', 'post type general name' ),
		'singular_name'      => _x( 'Товар', 'post type singular name' ),
		'add_new'            => _x( 'Добавить новый', 'prod' ),
		'add_new_item'       => __( 'Добавить новый товар' ),
		'edit_item'          => __( 'Редактировать товар' ),
		'new_item'           => __( 'Новый товар' ),
		'all_items'          => __( 'Все товары' ),
		'view_item'          => __( 'Смотреть товар' ),
		'search_items'       => __( 'Найти товар' ),
		'not_found'          => __( 'Товары не найдены' ),
		'not_found_in_trash' => __( 'Нет удаленных товаров' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Товары'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Пользовательский тип записей товаров',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields', 'prod_cat'),
		'has_archive'   => true,
		'menu_icon'     => 'dashicons-cart',
		'rewrite'       => array( 'slug' => 'prod' ),

	);
	register_post_type( 'prod', $args );   
}
add_action( 'init', 'my_custom_post_prod' );

function my_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['prod'] = array(
		0 => '', 
		1 => sprintf( __('Товар обновлен. <a href="%s">Посмотреть</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('Пользовательские поля обновлены.'),
		3 => __('Пользовательские поля обновлены.'),
		4 => __('Товар обновлен.'),
		5 => isset($_GET['revision']) ? sprintf( __('prod restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Товар опубликован. <a href="%s">Посмотреть</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('Товар сохранен.'),
		8 => sprintf( __('Товар отправлен. <a target="_blank" href="%s">Посмотреть</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Товар запланирован на: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Посмотреть</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('prod draft updated. <a target="_blank" href="%s">Посмотреть</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages' );


function my_taxonomies_prod() {
	$labels = array(
		'name'              => _x( 'Категории товаров', 'taxonomy general name' ),
		'singular_name'     => _x( 'Категория', 'taxonomy singular name' ),
		'search_items'      => __( 'Найти категорию' ),
		'all_items'         => __( 'Все категории' ),
		'parent_item'       => __( 'Родительская категория' ),
		'parent_item_colon' => __( 'Родительская категория:' ),
		'edit_item'         => __( 'Редактировать категорию' ), 
		'update_item'       => __( 'Обновить категорию' ),
		'add_new_item'      => __( 'Добавить новую категорию' ),
		'new_item_name'     => __( 'Новая категория' ),
		'menu_name'         => __( 'Категории' ),
	);
	$args = array(
		'labels'             => $labels,
		'hierarchical'       => true,
		'show_admin_column'  => true,
		'rewrite'            => array( 'hierarchical' => true, 'slug' => 'shop' ),
	);
	register_taxonomy( 'prod_cat', 'prod', $args );
}
add_action( 'init', 'my_taxonomies_prod', 0 );

function get_cat_name2( $cat_id ) {
	$cat_id = (int) $cat_id;
	$category = get_term( $cat_id, 'prod_cat' );
	if ( ! $category || is_wp_error( $category ) )
	return '';
	return $category->name;
}


?>