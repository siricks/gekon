<?php get_header(); ?>

	<section id="content">
		<section id="posts">
			<div class="breadc"><?php wp_reset_query(); if(function_exists('bcn_display')) { bcn_display(); } ?></div>
			<? if (isset($GLOBALS['h1'])){?>
				<h1><?=$GLOBALS['h1'];?></h1>
			<? } ?>
		<div class="post">
			<?php $cats_sorts=array(); $term_id = get_queried_object()->term_id; $terms = get_terms('prod_cat', 'parent='.$term_id.'&hide_empty=0');
			if ( !empty( $terms ) && !is_wp_error( $terms ) && (count($terms) > 0)) {
				foreach ($terms as $term) {
					$cats_sorts_n = Taxonomy_MetaData::get( 'prod_cat', $term->term_id, 'ar_cat_num' );
					if($cats_sorts_n == '') { $cats_sorts_n = 9999; }
					$cats_sorts[] = array("sort" => $cats_sorts_n, "cid" => $term->term_id,);
				}
				foreach ($cats_sorts as $key => $row) {
					$sort[$key]  = $row['sort'];
					$cid[$key] = $row['cid'];
				}
				array_multisort($sort, SORT_ASC, $cid, SORT_ASC, $cats_sorts);

				foreach ($cats_sorts as $cats_sort) {?>
					<div class="title"><noindex><?php echo get_cat_name2( $cats_sort['cid'] );?></noindex></div>
					<?php $args = array('post_type' => 'prod', 'showposts' => '1000', 'tax_query' => array( array( 'taxonomy' => 'prod_cat', 'field' => 'id', 'terms' =>  $cats_sort['cid'] ) ), );
					$news = new WP_query(); $news->query($args); ?>
					<?php while ($news->have_posts()) : $news->the_post(); ?>
					<div class="prod_list2">
						<div class="prod_list2_img"><?php echo raft_post_img_wp($post->ID, 1, 1); ?><?php echo raft_custum_field($post->ID, 'ar_price_min', 0, '<div class="prod_list2_p">', '</div>'); ?></div>
						<div class="prod_list2_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
						<div class="prod_list2_e"><noindex><?php echo raft_announce($post->post_content, 90); ?></noindex></div>
					</div>
					<?php endwhile; ?>
					
					<div class="clear"></div>
					<div class="entry entry_pad">
						<? if (isset($GLOBALS['seo_text'])){?>
						<div class="seo_text"><? echo $GLOBALS['seo_text'];?></div>
						<? } else {?>
						<div class="seo_text"><? the_field('seo_text'); ?></div>
						<? } ?>
					</div>
					
				<?php } ?>
			<?php } else { ?>
				<?php if (have_posts()) : ?>
			
				<?php $post = $posts[0]; ?>

					<h2><?php printf(__('%s'), single_cat_title('', false)); ?></h2>

				<?php while (have_posts()) : the_post(); ?>
			
					<div class="prod_list2">
						<div class="prod_list2_img"><?php echo raft_post_img_wp($post->ID, 1, 1); ?><?php echo raft_custum_field($post->ID, 'ar_price_min', 0, '<div class="prod_list2_p">', '</div>'); ?></div>
						<div class="prod_list2_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
						<div class="prod_list2_e"><?php echo raft_announce($post->post_content, 90); ?></div>
					</div>
			
				<?php endwhile; ?>
			
					<div class="p_nav"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
			
				<?php else : ?>
			
					<h2 class="center">Не найдено</h2>
					<p class="center">Извините, но вы ищете то, чего здесь нет.</p>
			
				<?php endif; ?>
			<?php } ?>
		</div>
		
		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</section>

<?php get_footer(); ?>