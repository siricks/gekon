<strong>Бетонные
    полы</strong> – являются основой всех промышленных полов. По сути, даже обычная фундаментная плита является бетонным полом. Бетонное основание является обязательным для любого вида промышленного покрытия, будь то топпинг, полимеры или ПВХ панели. И самое главное что, очень трудно имея некачественное основание, сделать качественный пол. В этом разделе мы выделили не только стандартные бетонные полы. Но также и все виды стяжек, которые могут служить основанием для последующего нанесения промышленных полов.
<div class="title_color titlePol">Виды бетонных полов</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/betonnye-poly/promyshlennye/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/betpol-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/betonnye-poly/promyshlennye/">Промышленные</a></div>
        </td>
        <td>
            <div><a href="/cementnye-styazhki/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/cementn-150x150.jpeg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/cementnye-styazhki/">Цементные стяжки</a></div>
        </td>
        <td>
            <div><a href="/rovniteli-dlya-pola/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/rovnit-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/rovniteli-dlya-pola/">Ровнители для пола</a></div>
        </td>
        <td>
            <div><a href="/promyshlennye-nalivnye-poly/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/promnalivpol-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/promyshlennye-nalivnye-poly/">Промышленные наливные</a></div>
        </td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">Бетонные полы по степени нагрузки:</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/05/sverhtyazh.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/sverhtyazh-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Сверхтяжелые нагрузки </a></div>
        </td>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/05/promnagr.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/promnagr-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Промышленные нагрузки </a></div>
        </td>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/05/srednnagr.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/srednnagr-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Средние нагрузки </a></div>
        </td>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/05/niz.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/niz-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Низкие нагрузки </a></div>
        </td>
    </tr>
    </tbody>
</table>
<div class="div-page-menu-new">
    <ul id="pagetabs" class="tabs-22">
        <li><a href="#" class="page-button-new">ОПИСАНИЕ</a></li>
        <li><a href="#" class="page-button-new">ПРЕИМУЩЕСТВА БЕТОННЫХ ПОЛОВ:</a></li>
        <li><a href="#" class="page-button-new">ЦЕНЫ НА БЕТОННЫЕ ПОЛЫ:</a></li>
        <li><a href="#" class="page-button-new">МАТЕРИАЛЫ</a></li>
    </ul>
</div>
<div id="pagepanes" class="panes" style="">
<div class="desc_desc">
    <div class="title_color titlePol">Коротко о бетонных полах</div>
    Как уже было сказано выше бетонные полы являются незаменимыми при любом строительстве. И если возможно избежать
    устройства полимерных полов, плитки, линолеума и т.д. то при любом строительстве вы обязательно столкнетесь с одним
    из указанных в этом разделе напольных покрытий.

    Именно поэтому, необходимо очень тщательно выбирать какой вид бетонного пола вы будете делать, ведь существует
    огромное количество вариантов. Мы не раз сталкивались, с тем, что заказчики экономили на стяжке, считая, что
    финишное покрытие может скрыть дефекты и качество основания, это не так! Не экономьте на бетоне! Каким бы дорогим и
    хорошим не был полимерный пол, при некачественном основании срок его эксплуатации снизиться в три раза, топпинг
    потрескается через месяц, а если вы решите просто оставить бетонный пол, то он начнет разрушаться через неделю.

    Бетонные полы в силу своих эксплуатационных характеристик очень редко оставляют финишным покрытием, так как он
    разрушается, активно пылит, впитывает в себя все жидкости и сложен в уборке. Наиболее частым покрытием поверх
    бетонных полов на промышленных объектах является полимерный пол или топпинг, на объектах коммерческого назначения
    используют стандартные паркеты, линолиумы, ламинаты и т.д. Однако вы всегда столкнетесь с бетонным полом.
    <div class="title_color titlePol">Бетонные полы</div>
    Бетонные полы делятся по классификации марка и класс бетона. Как правило, расшифровка марки бетона означает
    усреднённый предел прочности на сжатие, измеряющийся в кгс/кв.см, т е. чем выше марка, тем лучше бетон. А также
    бетонные стяжки делятся на два вида в зависимости от типа армирования: фибробетон или армированный сеткой.
    Например, для устройства полимерных полов, нужен бетон не меньше марки М300 и обязательно армированный сеткой или
    фиброй. Бетон необходимо затереть вертолетами, но обязательно затирать не до стекла, потому что в любом случае
    поверхность после достижения максимальной прочности (28 суток) еще подвергнется шлифовке.
    При устройстве полов с упрочненным верхним слоем, нужен обязательно бетон не ниже марки М300 и армированный сеткой,
    в случае если бетон будет ниже маркой, он неминуемо потрескается.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Цементные стяжки</div>
    Цементные стяжки являются самым простым и распространенным видом стяжек в гражданском строительстве. Низкая цена,
    простые материалы и технология укладки способствует повсеместному распространению цементно-песчаных стяжек. Однако
    при заказе цементной стяжки всегда стоит помнить о последующем финишном покрытие и нагрузках на пол. Ни в коем
    случае не делайте цементные стяжки в местах с нагрузками выше пешеходных. Ситуация когда заказчик, чтобы сэкономить
    и выровнять неровную фундаментную плиту заливает 3 см. слой стяжки, а потом просит покрыть ее полимерным полом,
    считая, что все будет хорошо случается повсеместно.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Ровнители для пола</div>
    Быстросохнущие самовыравнивающие смеси на цементной основе наливного типа, используются в качестве финишного
    ровнителя для пола, рассчитаны на слои от 5 до 50 мм. Используются там, где стяжка пола сделана не идеально гладко.
    Ровнители для пола можно разделить на начальные (которые наносятся толстым слоем, как правило такие ровнители для
    пола не обладают свойствами самовыравнивания) и вторые самовыравнивающиеся сухие строительные смеси (финишные),
    которые можно нанести тонким слоем, финишные ровнители обычно наносятся на цементную стяжку либо же на застывший
    слой первоначального ровнителя для пола. Благодаря широкому разнообразию материалов существуют ровнители с разной
    степенью прочности, поэтому они могут быть основой практически для любых напольных покрытий.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol"><a href="/">Промышленные наливные полы</a></div>
    Достаточно слабо распространены, по сути, являются аналогом полимерцементов. Механизированный способ заливки
    позволяется заливать большие площади за короткий промежуток времени. Также полы такого типа можно эксплуатировать
    через 2-3 часа после заливки. Однако реально на промышленных объектах редко используются в силу своей дороговизны и
    механических характеристик.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
</div>
    <div class="advantages">
        <div class="title_color titlePol">Преимущество бетонных полов</div>
        &nbsp;
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color2">Полимерцементы</div>
                <div class="col_pad">

                    -Единственный вариант основания для высоких нагрузок.
                    - Возможность делать любую толщину
                    - Подходит под как основание под любые нагрузки

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color3">Цементные стяжки:</div>
                <div class="col_pad">

                    - Низкая цена
                    - Простота в укладке

                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color4">Полимерцементы</div>
                <div class="col_pad">

                    - Возможность сделать тонкий слой
                    - Гладкость
                    - Разнообразие выбора по характеристикам
                    - Быстрый набор прочности

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color5">Полимерцементы</div>
                <div class="col_pad">

                    - Возможно наливать в несколько слоев
                    - Высокая прочность
                    - Идеальная гладкость
                    - Выдерживает значительные механические нагрузки
                    - Быстротвердеющий

                </div>
            </div>
        </div>
    </div>
    <div class="prices">
        <div class="title_color titlePol">Цены на бетонные полы</div>
        <table width="100%">
            <tbody>
            <tr>
                <td width="269"></td>
                <td width="83"><strong>до 100 кв.м.</strong></td>
                <td width="81"><strong>от 500 кв.м.</strong></td>
                <td width="88"><strong>от 1500 кв.м.</strong></td>
                <td width="84"><strong>от 5000 кв.м</strong></td>
            </tr>
            <tr>
                <td width="269"><strong>Бетонные полы </strong></td>
                <td width="83"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">работа на 1 кв.м.</td>
                <td width="83">700</td>
                <td width="81">550</td>
                <td width="88">450</td>
                <td width="84">350</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td colspan="4" width="336">В зависимости от требований к полам.</td>
            </tr>
            <tr>
                <td width="269"><strong>Цементные стяжки</strong></td>
                <td width="83"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">работа на 1 кв.м.</td>
                <td width="83">850</td>
                <td width="81">820</td>
                <td width="88">770</td>
                <td width="84">730</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td width="83">1800</td>
                <td width="81">1600</td>
                <td width="88">1400</td>
                <td width="84">1200</td>
            </tr>
            <tr>
                <td width="269"><strong>Ровнители для пола</strong></td>
                <td width="83"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">работа на 1 кв.м.</td>
                <td width="83">400</td>
                <td width="81">380</td>
                <td width="88">355</td>
                <td width="84">330</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td width="83">950</td>
                <td width="81">890</td>
                <td width="88">830</td>
                <td width="84">750</td>
            </tr>
            <tr>
                <td width="269"><strong>Промышленные наливные полы</strong></td>
                <td width="83"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">работа на 1 кв.м.</td>
                <td width="83">1200</td>
                <td width="81">1050</td>
                <td width="88">950</td>
                <td width="84">870</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td width="83">2100</td>
                <td width="81">1950</td>
                <td width="88">1800</td>
                <td width="84">1600</td>
            </tr>
            </tbody>
        </table>
    </div>
<div class="materials">
    <div class="title_color">Основные материалы</div>
    <table class="table_null" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/5.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П30 грунт – полиуретановый грунт для прочных оснований</td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/6.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П40 грунт. Полиуретановый грунт для рыхлых оснований (ЦПС и низкокачественный бетон)
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/4.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П60. Полиуретановый однокомпонентный состав для устройства тонкослойного полимерного
                покрытия
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/3.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон НП1. Полиуретановая двухкомпонентная цветная композиция для устройства наливных
                полов,
                не содержит растворителей
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/2.png" alt="" width="200"/>
            </td>
            <td valign="top">ЦЕМЕЗИТ УР 69 – трехкомпонентное покрытие пола для тяжелого и очень тяжелого режима
                эксплуатации толщиной 6 — 9 мм
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/1.png" alt="" width="200"/>
            </td>
            <td valign="top">Цемезит 69рем – трехкомпонентный Полимерминеральный состав для выравнивания и ремонта
                оснований
                под покрытие пола
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
[contact-form-7 id="2487" title="заказать обратный звонок"]
<script> jQuery(document).ready(function () {
        jQuery("ul.tabs-22").tabs("div.panes > div");
    });
</script>