<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

include (TEMPLATEPATH . '/plugins/metabox/init.php');

include (TEMPLATEPATH . '/plugins/metabox/Taxonomy_MetaData_CMB2.php');
include (TEMPLATEPATH . '/plugins/metabox/theme-options-cmb.php');


/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function cmb2_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

add_filter( 'cmb2_meta_boxes', 'ar_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function ar_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'ar_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['ar_metabox'] = array(
		'id'         => 'ar_metabox',
		'title'      => __( 'Дополнительные поля', 'cmb2' ),
		'object_types'      => array( 'prod', ),
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true,
		'fields'     => array(


			array(
				'name' => __( 'Цена', 'cmb2' ),
				'id'   => $prefix . 'price_min',
				'type' => 'text',
			),

			array(
				'name'         => __( 'Доп. изображения', 'cmb2' ),
				'id'           => $prefix . 'images',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ),
			),


		),
	);


	$meta_boxes['ar_metabox2'] = array(
		'id'         => 'ar_metabox2',
		'title'      => __( 'Дополнительные поля', 'cmb2' ),
		'object_types'      => array( 'page', 'post'),
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true,
		'fields'     => array(


			array(
				'name'         => __( 'Изображения в подвале', 'cmb2' ),
				'id'           => $prefix . 'images_bot',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ),
			),


		),
	);


	/**
	 * SLIDER
	 */
	$meta_boxes['about_page_metabox'] = array(
		'id'           => 'about_page_metabox',
		'title'        => __( 'Главная настройки', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true,
		'show_on'      => array( 'id' => array( 2, ) ), // Specific post IDs to display this metabox
		'fields'       => array(
			array(
				'name'         => __( 'Слайды', 'cmb2' ),
				'id'          => $prefix . 'slider',
				'type'        => 'group',
				'options'     => array(
					'add_button'    => __( 'Новый слайд', 'cmb2' ),
					'remove_button' => __( 'Удалить слайд', 'cmb2' ),
					'sortable'      => true,
				),
				'fields'      => array(
					array(
						'name' => __( 'Изображение', 'cmb2' ),
						'desc'         => __( 'Размер 986px на 340px', 'cmb2' ),
						'id'   => 'image',
						'type' => 'file',
					),
					array(
						'name' => 'Текст кнопки',
						'id'   => 'text1',
						'type' => 'text',
					),
					array(
						'name' => __( 'Ссылка', 'cmb2' ),
						'id'   => 'url',
						'type' => 'text_url',
					),
					array(
						'name' => 'Текст на картинке',
						'id'   => 'text2',
						'type' => 'textarea',
					),
				),
			),


		)
	);


	/**
	 * Category
	 */


	$meta_box = array(
		'id'         => 'cat_options',
		'show_on'    => array( 'key' => 'options-page', 'value' => array( 'unknown', ), ),
		'show_names' => true,
		'fields'     => array(
			array(
				'name' => __( 'Изображение категории', 'cmb2' ),
				'id'   => $prefix . 'image',
				'type' => 'file',
			),

			array(
				'name' => __( 'Сортировка (только цифра)', 'cmb2' ),
				'id'   => $prefix . 'cat_num',
				'type' => 'text',
			),

		)
	);

	$cats = new Taxonomy_MetaData_CMB2( 'prod_cat', $meta_box, __( 'Опции для категорий', 'taxonomy-metadata' ), $overrides );



	/**
	 * Metabox for an options page. Will not be added automatically, but needs to be called with
	 * the `cmb2_metabox_form` helper function. See wiki for more info.
	 */
	$meta_boxes['options_page'] = array(
		'id'      => 'ar_theme_option',
		'title'   => __( 'Настройки темы', 'cmb2' ),
		'show_on' => array( 'options-page' => array( $prefix . 'theme_options', ), ),
		'fields'  => array(

			array(
				'name'         => __( 'Изображения в подвале', 'cmb2' ),
				'id'           => $prefix . 'images_bot_other',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ),
			),

		)
	);


	return $meta_boxes;
}
