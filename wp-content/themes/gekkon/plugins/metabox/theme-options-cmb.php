<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class Ar_Theme_Option_Admin {
	private $key = 'ar_theme_options';

	private $metabox_id = 'ar_theme_option';

	protected $title = '';

	protected $options_page = '';

	public function __construct() {
		$this->title = __( 'Настройки темы', 'myprefix' );
	}

	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
	}

	public function init() {
		register_setting( $this->key, $this->key );
	}

	public function add_options_page() {
		$this->options_page = add_submenu_page( 'themes.php', $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
	}

	public function admin_page_display() {
		?>
		<div class="wrap cmb2_options_page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}

	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		throw new Exception( 'Invalid property: ' . $field );
	}
}

function ar_theme_option_admin() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new Ar_Theme_Option_Admin();
		$object->hooks();
	}
	return $object;
}

function ar_get_theme_option( $key = '' ) {
	return cmb2_get_option( ar_theme_option_admin()->key, $key );
}

ar_theme_option_admin();