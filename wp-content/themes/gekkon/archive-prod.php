<?php get_header(); ?>

	<section id="content">
		<section id="posts">

			<div class="breadc"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></div>

			<div class="post">
				<h1>Категории</h1>
				<div class="cat_lists">
					<?php $terms = get_terms('prod_cat', 'parent=0&hide_empty=0');
					if ( !empty( $terms ) && !is_wp_error( $terms ) ) {
						foreach ($terms as $term) { $term_img = Taxonomy_MetaData::get( 'prod_cat', $term->term_id, 'ar_image_id' ); ?>
							<div class="cat_list">
								<div class="cat_list_img"><a href="<?php echo get_term_link( $term ); ?>"><?php echo raft_post_img_wp($term, 0, 1, 'thumbnail', '', '', $term_img, 2); ?></a></div>
								<div class="cat_list_t"><a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a></div>
							</div>
						<?php }
					} ?>
					<div class="clear"></div>
				</div>
			</div>


		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
<?php include (TEMPLATEPATH . '/primeri.php'); ?>
	</section>

<?php get_footer(); ?>