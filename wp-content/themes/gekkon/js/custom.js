﻿jQuery(document).ready(function(){

	jQuery('table').wrap("<div class='table_container'></div>");

	jQuery('.nav_top > ul > li').hover(
	function() {
		jQuery(this).find('ul').fadeIn(90);
	},
	function() {
		jQuery(this).find('ul').fadeOut(90);
	});


	jQuery('.show_nav').click(function() {
		jQuery('.nav_top').slideToggle(100);
		return false;
	});


	jQuery('a.modal').click(function(e) {
		e.preventDefault();
		var id = jQuery(this).attr('href');
		var maskHeight = jQuery(document).height();
		var maskWidth = jQuery(window).width();
		jQuery('.modal_mask').css({'width':maskWidth,'height':maskHeight});
		jQuery('.modal_mask').fadeIn(0);	
		jQuery('.modal_mask').fadeTo(0,0.65);

		var winH = jQuery(window).height();
		var winHt = jQuery(document).scrollTop();
		var winW = jQuery(window).width();
		var modh = jQuery(id).innerHeight();
		var modw = jQuery(id).innerWidth();
              	if (modh > winH) {
			jQuery(id).css('top', winHt);
		} else {
			jQuery(id).css('top', winHt + winH/2-modh/2);
		}
		jQuery(id).css('left', winW/2-modw/2);

		jQuery(id).fadeIn(200);
	});

	jQuery('.modal_fast').click(function(e) {
		e.preventDefault();

		var fast_cart_t = jQuery(this).parents('.prod_parent').find('.prod_parent_t').text();
		jQuery('#fast_cart_t').val(fast_cart_t);

		var fast_cart_url = jQuery(this).parents('.prod_parent').find('.prod_parent_u').text();
		jQuery('#fast_cart_url').val(fast_cart_url);


		var id = jQuery(this).attr('href');
		var maskHeight = jQuery(document).height();
		var maskWidth = jQuery(window).width();
		jQuery('.modal_mask').css({'width':maskWidth,'height':maskHeight});
		jQuery('.modal_mask').fadeIn(0);	
		jQuery('.modal_mask').fadeTo(0,0.5);

		var winH = jQuery(window).height();
		var winHt = jQuery(document).scrollTop();
		var winW = jQuery(window).width();
		var modh = jQuery(id).innerHeight();
		var modw = jQuery(id).innerWidth();
              	if (modh > winH) {
			jQuery(id).css('top', winHt);
		} else {
			jQuery(id).css('top', winHt + winH/2-modh/2);
		}
		jQuery(id).css('left', winW/2-modw/2);

		jQuery(id).fadeIn(200);
	});


	jQuery('.modal_close').click(function (e) {
		e.preventDefault();
		jQuery('.modal_mask, .modal_win').hide();
	});		

	jQuery('.modal_mask').click(function () {
		jQuery(this).hide();
		jQuery('.modal_win').hide();
	});


	jQuery("ul.tabs").tabs("> .tabs_pane");


// Fancybox начало

	jQuery.fn.getTitle = function() {
		var arr = jQuery("a.fancybox");
		jQuery.each(arr, function() {
			var title = jQuery(this).children("img").attr("title");
			jQuery(this).attr('title',title);
		})
	}
	var thumbnails = 'a:has(img)[href$=".bmp"],a:has(img)[href$=".gif"],a:has(img)[href$=".jpg"],a:has(img)[href$=".jpeg"],a:has(img)[href$=".png"],a:has(img)[href$=".BMP"],a:has(img)[href$=".GIF"],a:has(img)[href$=".JPG"],a:has(img)[href$=".JPEG"],a:has(img)[href$=".PNG"]';
	jQuery(thumbnails).each(function(){
		if(jQuery(this).has("img")) {
			if(jQuery(this).hasClass('fancybox')) {
				jQuery(this).getTitle();
			} else {
				jQuery(this).addClass("fancybox").attr("rel","fancybox").getTitle();
			}
		}
	});  
	jQuery("a.fancybox").fancybox({
		'padding': 2,
		'overlayColor': "#000000",
		'overlayOpacity': 0.5,
		'centerOnScroll': true
	});

// Fancybox конец
	// Плавающее меню слева
		var	$sidebar = jQuery("#sidebar");
		$sidebar.after('<div class="menu-air"></div>');
		jQuery(".sb_nav, .sb_rasch").clone().appendTo(".menu-air");
		var $window = jQuery(window),
			windH = $window.height(),
			menuTop = $sidebar.outerHeight() + $sidebar.offset().top,
			$menu = jQuery(".menu-air"),
			$posts = jQuery("#posts"),
			menuH = $menu.outerHeight(),
			posBot = $posts.offset().top + $posts.outerHeight();
		var scT = jQuery(document).scrollTop();
		$menu.parent().css("position","relative");
		$menu.hide();
		if ((scT > menuTop) && (menuH + 40 < windH) && (scT + 20 + menuH < jQuery("#posts").offset().top + jQuery("#posts").outerHeight())) {
			$menu.fadeIn("slow");
			$menu.offset({top: menuTop + 300});
		};
		/*if ((menuH + 40 < windH) && (menuTop + 20 + menuH < posBot)) {
				if ($menu.css("display") != "block") 
					{$menu.fadeIn("slow");};
			}else{
				if ($menu.css("display") != "none") 
					{$menu.fadeOut("fast");};
			};*/
		jQuery(window).bind('scroll resize',function(){
			windH = $window.height();
			menuH = $menu.outerHeight();
			scT = jQuery(document).scrollTop();
			posBot = $posts.offset().top + $posts.outerHeight();
			if ((scT > menuTop) && (menuH + 40 < windH) && (scT + 20 + menuH < jQuery("#posts").offset().top + jQuery("#posts").outerHeight())) {
				if ($menu.css("display") != "block") 
					{$menu.fadeIn("slow");};
			}else{
				if ($menu.css("display") != "none") 
					{$menu.fadeOut("fast");};
			};
			/*if ((menuH + 40 < windH) && (menuTop + 20 + menuH < posBot)) {
				if ($menu.css("display") != "block") 
					{$menu.fadeIn("slow");};
			}else{
				if ($menu.css("display") != "none") 
					{$menu.fadeOut("fast");};
			};*/
			if (scT < menuTop) {
				$menu.offset({top: menuTop});
			} else if (menuH + 20 + scT > posBot - 20) {
				$menu.offset({top: posBot - 20 - menuH});
			} else {
				$menu.offset({top: scT + 200});
			};
			
		});
});