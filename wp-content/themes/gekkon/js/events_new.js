/* ver. 3.2.0 */
var ingEventsConfig = {
  debug    :true, //debug mode
  idYM     :['45710115'],   //Yandex Metrika id array
  idGA     :[''],   //GA tracking name array
  idGT     :['UA-110343448-1']    //GA tracking id array
};
function ingEvents(params, config){
  var debug    = config.debug;
  var counters = [];
  var idYM     = config.idYM;
  var idGA     = config.idGA;
  var idGT     = config.idGT;
  
  //Search Yandex Metrika tracking ids
  if (!idYM.length && window.Ya && window.Ya._metrika) {
    var key;
    for (key in window.Ya._metrika.hitParam) {
      if (!window.Ya._metrika.hitParam.hasOwnProperty(key)) { continue; }
      idYM.push(parseInt(key));
    }
  }
  //Search GA tracking names and ids
  if (typeof ga !=="undefined" && !idGT.length && !idGA.length) {
    var ga_trackers = [];
    ga(function() {
      ga_trackers = ga.getAll();
      ga_trackers.forEach(function(item, i, arr) {
        var ga_name = item.get('name');
        var ga_tid  = item.get('trackingId');
        if(ga_name == 't0') { idGA.push('send'); }
        if(/^(gtag_UA_.*)$/g.exec(ga_name) === null && ga_name != 't0') { idGA.push(ga_name + '.send'); }
        idGT.push(item.get('trackingId'));
      });
    });
  }

  // Cheking parameters
  if (typeof params !== "undefined"){
    // Cheking GA parameters
    if (typeof params.category !== "undefined" && typeof params.action !== "undefined" && typeof params.label !== "undefined" ) {
      // Search pageTracker
      if (typeof pageTracker !== "undefined"){
        pageTracker._trackEvent(params.category, params.action, params.label);
        ingEventsCallback('Google', params, debug);
        counters.push('pageTracker');
      }
      // Search tracker ga.js
      if (typeof _gaq !== "undefined"){
        _gaq.push(['_trackEvent', params.category, params.action, params.label]);
        ingEventsCallback('Google', params, debug);
        counters.push('_gaq');
      }
      // Search tracker analytics.js
      if (typeof ga !=="undefined"){
        for(i=0;i<idGA.length;i++) {
          ga(idGA[i], 'event', params.category, params.action, params.label,{hitCallback: ingEventsCallback('Google', params, debug)});
          counters.push('ga(' + idGA[i] + ')');
        }
      }
      // Search tracker gtag.js
      if (typeof gtag !== "undefined"){
        gtag('event', params.action, {'event_category': params.category,'event_label': params.label});
        counters.push('gtag');
      }
    }
    if (typeof params.ga_page !== "undefined"  ) {
      // Search tracker ga.js
      if (typeof _gaq !== "undefined"){
        _gaq.push(['_trackPageview', params.ga_page]);
        ingEventsCallback('Google', params, debug);
        counters.push('_gaq');
      }
      // Search tracker analytics.js
      if (typeof ga !=="undefined"){
        for(i=0;i<idGA.length;i++) {
          ga(idGA[i], 'pageview', params.ga_page, {hitCallback: ingEventsCallback('Google', params, debug)});
          counters.push('ga(' + idGA[i] + ')');
        }
      }
      // Search tracker gtag.js
      if (typeof gtag !=="undefined"){
        for(i=0;i<idGT.length;i++) {
          gtag('event', 'page_view', { 'send_to': idGT[i], 'page_path': params.ga_page });
          ingEventsCallback('Google', params, debug);
          counters.push('gtag(' + idGT[i] + ')');
        }
      }
    }
    
    if (typeof params.ya_label == "undefined" && typeof params.ya_page !== "undefined") {
      params.ya_label = params.ya_page;
    }
    // Cheking YM parameters
    if (typeof params.ya_label !== "undefined") {
      if (typeof params.goalParams == "undefined") { params.goalParams = {};}
      for(i=0;i<idYM.length;i++) {
        // Checking YM
        if (typeof window['yaCounter'+idYM[i]] !=="undefined"){
          // Call YM reachGoal
          window['yaCounter'+idYM[i]].reachGoal(params.ya_label, params.goalParams, ingEventsCallback('Yandex', params, debug));
          counters.push('yaCounter'+idYM[i]);
        }
      }
    }
    ingEventsLog(counters, debug);
  }
}
// Log trackers on the page
function ingEventsLog(counters, debug){
  if(debug === true) {
    console.log('Events script version: 3.2.0\n')
    if(counters.length === 0) {
      console.log('Сounters:\n\tnone');
    } else {
      console.log('Сounters:\n\t'+counters);
    }
  }
}
// GA and YM callback function
function ingEventsCallback(name, params, debug){
  var msg ='';
  if(debug === true) {
    if(typeof params.ga_page !== "undefined" || typeof params.ya_page !== "undefined") {
      msg = name+' pageview reached:\n\t';
    } else {
      msg = name+' event reached:\n\t';
    }
    if(name == 'Yandex') {
      if(typeof params.ya_page !== "undefined") {
        msg = msg + '[ya_page : '+params.ya_label+']\n';
      } else {
        msg = msg + '[ya_label : '+params.ya_label+']\n';
      }
    }
    if(name == 'Google') {
      if(typeof params.ga_page !== "undefined") {
        msg = msg + '[ga_page : '+params.ga_page+']\n';
      } else {
        msg = msg + '[category : '+params.category+']\n\t[action   : '+params.action+']\n\t[label    : '+params.label+']\n';
      }
    }
    console.log(msg);
  }
}
// Lagacy proxy functions
function ing_events(params) {
  try{
    if (typeof params.ya_label == "undefined" && typeof params.label !== "undefined") {
      params.ya_label = params.label;
    }
    ingEvents(params, ingEventsConfig);
  } catch(e){console.log(e.message);}
}
function ing_pageview(params) {
  try{
    ingEvents({ga_page:params.page, ya_page:params.label}, ingEventsConfig);
  } catch(e){console.log(e.message);}
}