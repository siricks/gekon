
<!--<div class="title_color titlePol">Виды упрочнения полов:</div>-->
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/topping/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/topping-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/topping/">Топпинг</a></div>
        </td>
        <td>
            <div><a href="/flyuatirovanie/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/fluatir-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/flyuatirovanie/">Флюатирование</a></div>
        </td>
        <td>
            <div><a href="chto-takoe-polirovannyj-beton/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/polirbet-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="chto-takoe-polirovannyj-beton/">Полированный бетон</a></div>
        </td>
        <td>
            <div><a href="/presovannyj-beton/">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/shtampov-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/presovannyj-beton/">Печатный бетон</a></div>
        </td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">Упрочненные полы по типу объектов:</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/04/po_tipu_parking2.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/po_tipu_parking2-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Паркинги и производственные помещения</a></div>
        </td>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/04/po_tipu_ofis3.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/po_tipu_ofis3-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Офисные и торговые площади</a></div>
        </td>
        <td>
            <div><a href="https://gekkonsystem.ru/wp-content/uploads/2015/05/722_big.jpg">
                    <img src="https://gekkonsystem.ru/wp-content/uploads/2015/05/722_big-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Загородное строительство</a></div>
        </td>
    </tr>
    </tbody>
</table>

<div class="div-page-menu-new">
    <ul id="pagetabs" class="tabs-22">
        <li><a href="#" class="page-button-new">ОПИСАНИЕ</a></li>
        <li><a href="#" class="page-button-new">ПРЕИМУЩЕСТВА ПОЛИМЕРНЫХ ПОЛОВ:</a></li>
        <li><a href="#" class="page-button-new">Цены на упрочненные полы:</a></li>
        <li><a href="#" class="page-button-new">МАТЕРИАЛЫ</a></li>
    </ul>
</div>
<div id="pagepanes" class="panes" style="">
    <div class="desc_desc">
        <div class="title_color titlePol">Коротко об упрочненных полах</div>
        В вступлении мы поговорили о необходимости упрочнять бетонную поверхность. Но как же правильно выбрать способ
        упрочнения?

        Здесь заказчик отталкивается от бюджета и, конечно, предпочтительного результата. Важно понимать, что выбор любого
        способа – изначально правильный выбор, ведь Вы гарантированно защитите стяжку и, что не менее важно, получите в
        какой-то степени произведение искусства на своем объекте.

        Поговорим немного подробнее о доступных видах упрочнения:
        <div class="title_color titlePol">Топпинг</div>
        Смесь, упрочняющая верхний слой бетонного покрытия, который чаще всего используют в производственных помещениях,
        складах, паркингах, где предполагается интенсивное движение или тяжелую погрузочную технику. Топ технология
        эффективна, ведь уже через неделю прочность и ударостойкость бетонного пола увеличиваются более чем в два раза, а
        износостойкость – в восемь.

        Поставляется как правило в виде сухой смеси, в основе которой цемент с примесями – кварцем, корундом, металлическими
        частицами. Выбор наполнения зависит от предполагаемых нагрузок – для умеренных советуют наполнение кварцем, при
        интенсивных и тяжелых – корундом и частицами.

        В то же время топ уязвим для химических реагентов. Для помещений, где предполагаются подобные нагрузки, лучше
        использовать полимерные покрытия или полированный бетонный пол.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="/texnologiya-toppinga/">Технология устройства</a></div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
        <div class="title_color titlePol">Флюатрирование</div>
        Способ упрочнения бетона нанесением жидких составов, основой которых являются полимеры. В данном случае будет
        справедливо назвать полимерный материал флюатом, который вступает в химическую реакцию с базой нанесения.

        Покрытие пола полимерной грунтовкой вызывает гермитизацию стяжки. Так, укрепляется и обеспыливается поверхность,
        увеличивается срок службы и даже уменьшается вероятность образования трещин.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="/texnologiya-flyuatirovaniya/">Технология устройства</a>
                </div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
        <div class="title_color titlePol">Полированный бетон</div>
        Покрытие, на данный момент наиболее популярное в западных странах. Своей популярности оно обязано исключительной
        прочности, износостойкости, стойкости к всевозможным химикатам и неограниченному числу вариантов исполнения. Кроме
        того полированный бетон попал в тренд всеобщего стремления к натуральности. Бетонное покрытие раскрывает свою
        естественную красоту, так как «срезается» верхний слой, обнажая красивую структуру стяжки, и наполировывается до
        блеска.

        Так, бетонная стяжка, как и дорогие натуральные полы из мрамора или гранита приобретает абсолютно уникальный внешний
        вид.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="/texnologiya-polucheniya-polirovannogo-betona/">Технология
                        устройства</a></div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
        <div class="title_color titlePol">Штампованный бетон</div>
        Пока весьма слабо распространенный тип покрытия в России. Однако на западе, и в особенности в США, покрытие
        приобрело армию поклонников. Штампованный бетон используют в оформлении огромных городских площадей, тротуаров
        выставочных залов, ресторанов, садовых дорожек – всех тех мест, где необходимо совместить уникальность и красивую
        кладку декоративного камня, дубовых досок и надежность монолитной бетонной стяжки.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="/texnologiya-shtampovannogo-betona/">Технология
                        устройства</a>
                </div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="title_color titlePol titlePol2">Преимущества полимерных полов:</div>
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color2">Топпинг</div>
                <div class="col_pad">

                    - Можно получить разную фактуру
                    - Экономичность и быстрота сдачи
                    - Высокая ударо и износостойкость
                    - Исключается отслоение верхнего слоя
                    - Пол с топпингом обладает антистатическими свойствами

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color3">Флюатирование</div>
                <div class="col_pad">

                    - Низкая цена
                    - Простота нанесения
                    - Ремонтопригодно
                    - Пол не пылит

                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color4">Полированный бетон</div>
                <div class="col_pad">

                    - Исключительный внешний вид
                    - Отменная прочность
                    - Возможность нанесения рисунков и логотипов
                    - «Зеркальная» гладкость поверхности
                    - Низкие эксплуатационные затраты

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color5">Штампованный бетон</div>
                <div class="col_pad">

                    - Невысокая стоимость
                    - Несет и укрепляющую и декоративную функцию
                    - Множество вариантов позволят соответствовать любому дизайнерскому проекту
                    - Долговечность и основательность

                </div>
            </div>
        </div>
    </div>
<div class="prices">
    <div class="title_color titlePol">Цены на упрочненные полы</div>
    Рассчитать цену он-лайн Вам, а также подобрать оптимальное для Вас решение поможет наш калькулятор.

    Цены предоставленные ниже рассчитаны исходя из подготовленного основания и толщины плиты в 100 мм.
    <table width="100%">
        <tbody>
        <tr>
            <td width="269"></td>
            <td width="83"><strong>до 100 кв.м.</strong></td>
            <td width="81"><strong>от 1000 кв.м.</strong></td>
            <td width="88"><strong>до 5000 кв.м.</strong></td>
            <td width="84"><strong>от 5000 кв.м</strong></td>
        </tr>
        <tr>
            <td width="269"><strong>Топпинг</strong></td>
            <td width="83"></td>
            <td width="81"></td>
            <td width="88"></td>
            <td width="84"></td>
        </tr>
        <tr>
            <td width="269">работа на 1 кв.м.</td>
            <td width="83"> 750</td>
            <td width="81">650</td>
            <td width="88"> 540</td>
            <td width="84"> 490</td>
        </tr>
        <tr>
            <td width="269">Цена 1 кв.м. под ключ</td>
            <td width="83">1500</td>
            <td width="81">1400</td>
            <td width="88">1270</td>
            <td width="84">1180</td>
        </tr>
        <tr>
            <td width="269"><strong>Флюатирование</strong></td>
            <td width="83"></td>
            <td width="81"></td>
            <td width="88"></td>
            <td width="84"></td>
        </tr>
        <tr>
            <td width="269">работа на 1 кв.м.</td>
            <td width="83"> 550</td>
            <td width="81">490</td>
            <td width="88">430</td>
            <td width="84">380</td>
        </tr>
        <tr>
            <td width="269">Цена 1 кв.м. под ключ</td>
            <td width="83"> 900</td>
            <td width="81">820</td>
            <td width="88">690</td>
            <td width="84">590</td>
        </tr>
        <tr>
            <td width="269"><strong>Полированный бетон</strong></td>
            <td width="83"></td>
            <td width="81"></td>
            <td width="88"></td>
            <td width="84"></td>
        </tr>
        <tr>
            <td width="269">работа на 1 кв.м.</td>
            <td width="83"> 600</td>
            <td width="81">350</td>
            <td width="88">280</td>
            <td width="84">190</td>
        </tr>
        <tr>
            <td width="269">Цена 1 кв.м. под ключ</td>
            <td width="83"> 1400</td>
            <td width="81">1150</td>
            <td width="88">990</td>
            <td width="84"> 830</td>
        </tr>
        <tr>
            <td width="269"><strong>Штампованный бетон</strong></td>
            <td width="83"></td>
            <td width="81"></td>
            <td width="88"></td>
            <td width="84"></td>
        </tr>
        <tr>
            <td width="269">работа на 1 кв.м.</td>
            <td width="83"> 800</td>
            <td width="81">700</td>
            <td width="88">600</td>
            <td width="84">500</td>
        </tr>
        <tr>
            <td width="269">Цена 1 кв.м. под ключ</td>
            <td width="83"> 2000</td>
            <td width="81">1700</td>
            <td width="88">1590</td>
            <td width="84">1350</td>
        </tr>
        </tbody>
    </table>
    &nbsp;
</div>

<div class="materials">
    <div class="title_color">Основные материалы</div>
    <table class="table_null" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/5.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П30 грунт – полиуретановый грунт для прочных оснований</td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/6.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П40 грунт. Полиуретановый грунт для рыхлых оснований (ЦПС и низкокачественный бетон)
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/4.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон П60. Полиуретановый однокомпонентный состав для устройства тонкослойного полимерного
                покрытия
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/3.png" alt="" width="200"/>
            </td>
            <td valign="top">Геккон НП1. Полиуретановая двухкомпонентная цветная композиция для устройства наливных
                полов,
                не содержит растворителей
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/2.png" alt="" width="200"/>
            </td>
            <td valign="top">ЦЕМЕЗИТ УР 69 – трехкомпонентное покрытие пола для тяжелого и очень тяжелого режима
                эксплуатации толщиной 6 — 9 мм
            </td>
        </tr>
        <tr>
            <td width="220"><img src="https://gekkonsystem.ru/wp-content/uploads/2015/04/1.png" alt="" width="200"/>
            </td>
            <td valign="top">Цемезит 69рем – трехкомпонентный Полимерминеральный состав для выравнивания и ремонта
                оснований
                под покрытие пола
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
[contact-form-7 id="2487" title="заказать обратный звонок"]
<script> jQuery(document).ready(function () {
        jQuery("ul.tabs-22").tabs("div.panes > div");
    });
</script>
При решении задачи устройства промышленных полов заказчик в большинстве случаев сталкивается с использованием бетонной стяжки. Такое покрытие сегодня считается базой под применение одной из современных технологий в связи с пористой структурой, которая позволяет проникновение воды, химических веществ – в том числе масла и смазок. Если не защитить уязвимую бетонную стяжку, она окажется недолговечной – верхний слой ее местами разрушится, в помещении будет пыльно, приятный внешний вид останется в прошлом.
