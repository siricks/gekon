<?php if ($images_bot != '') { ?>
    <div class="primeri">
        <?php foreach ($images_bot as $images_bot_id => $image) { ?>
            <div class="primer"><a rel="primeri"
                                   href="<?php echo raft_post_img_wp($post->ID, 0, 0, 'img260', '', '', $images_bot_id); ?>"><?php echo raft_post_img_wp($post->ID, 0, 1, 'img260', '', '', $images_bot_id); ?></a>
            </div>
        <?php } ?>
        <div class="clear"></div>
    </div>
<?php } else { ?>
    <?php $images_bot = ar_get_theme_option('ar_images_bot_other');
    if (isset($images_bot) && $images_bot !== false):
    ?>
        <div class="primeri">
            <?php if ($_SERVER['REQUEST_URI'] != '/') { ?>
                <div class="primeri_t">ПРИМЕРЫ РАБОТ</div><?php } ?>
            <?php foreach ($images_bot as $images_bot_id => $image) { ?>
                <div class="primer"><a rel="primeri"
                                       href="<?php echo raft_post_img_wp($post->ID, 0, 0, 'img260', '', '', $images_bot_id); ?>"><?php echo raft_post_img_wp($post->ID, 0, 1, 'img260', '', '', $images_bot_id); ?></a>
                </div>
            <?php } ?>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
<?php } ?>
