<? //print_r($_SERVER);
//if($_SERVER['HTTP_X_REQUEST_SCHEME'] == "http" || $_SERVER['HTTP_HOST']=='www.gekkonsystem.ru'){
//    $redirect = 'https://gekkonsystem.ru'. $_SERVER['REQUEST_URI'];
//    header('HTTP/1.1 301 Moved Permanently');
//    header('Location: ' . $redirect);
//    exit();
//}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110343448-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-110343448-1');
</script>
<meta name='yandex-verification' content='75dca8b451f5dd02' />
<meta name="google-site-verification" content="nADkZ1eq5hP1sTZYj0mc7HjHNi_--ZzlKIqVIkrDZm0" />
<meta name="mailru-domain" content="kYuzgaK0JXpAQQPq" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php
	$title = the_title( '', '', false ).' – Геккон';
	//print_r(get_queried_object()->term_id);
	if (get_queried_object()->term_id != ''){
		$title = get_option('prod_cat_'.get_queried_object()->term_id.'_title');
		$description = get_option('prod_cat_'.get_queried_object()->term_id.'_description');
		$keywords = get_option('prod_cat_'.get_queried_object()->term_id.'_keywords');
		$GLOBALS['h1'] = get_option('prod_cat_'.get_queried_object()->term_id.'_h1');
		$GLOBALS['seo_text'] = get_option('prod_cat_'.get_queried_object()->term_id.'_seo_text');
		if ($title==''){$title = get_queried_object()->name.' – Геккон';}
		}
	?>
	<title><?php if (get_field('title')) {the_field('title');} else {echo $title;} ?></title>
	<meta name="description" content="<?php if(strpos($_SERVER['REQUEST_URI'], '/prod/') !== false){
    $title = the_title( '', '', false ); if (get_field('h1')) {echo get_field('h1').': компания «Геккон» в Санкт-Петербурге.';} else {echo $title.': компания «Геккон» в Санкт-Петербурге.';};} elseif
	(get_field('description')) {the_field('description');} else {echo $description;} ?>" />
	<meta name="keywords" content="<?php if (get_field('keywords')) {the_field('keywords');} else {echo $keywords;} ?>" />
	
	<?php wp_enqueue_script("jquery"); ?>
	<?php wp_head(); ?>
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--[if lt IE 9]><script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script><![endif]-->

	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style760.css" media="screen and (max-width: 1000px)">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style320.css" media="screen and (max-width: 759px)">
	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox.js?v=2.1.3"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox.css?v=2.1.2" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-media.js?v=1.0.5"></script>
	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle2.js"></script>

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.tools.min.tabs.js"></script>

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/events_new.js"></script>
	
<meta name="yandex-verification" content="7483eaca4939a3b5" />
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47687470 = new Ya.Metrika({
                    id:47687470,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47687470" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</head>
<body>
<div class="bg_top"><div class="bg_bot"><div class="bg_bot2">
<div class="wrap">
	<header>
		<div class="header_c">
			<div class="logo"><a href="https://gekkonsystem.ru/"><span>Строительно-торговый дом</span></a></div>
			<div class="header_r">
<!--				<div class="M M_soc">-->
<!--					<ul>-->
<!--						<li><a class="M_soc__vk" href="https://vk.com/public98137517" rel="nofollow">Вконтакте</a></li>-->
<!--						<li><a class="M_soc__tw" href="#">Твиттер</a></li>-->
<!--						<li><a class="M_soc__ig" href="https://instagram.com/gekkonsystem/" rel="nofollow">Инстаграм</a></li>-->
<!--					</ul>-->
<!--				</div>-->
                <a href="tel:88126433302"><div class="top_tel"> (812) 643-33-02</div></a>
				<div class="top_zz">
					<a class="modal" href="#top_zz">Заказать звонок</a>

				</div>
<!--				--><?php //get_search_form(); ?>
			</div>
			<div class="clear"></div>
		</div>
		<span class="show_nav"></span>
		<nav class="nav_top"><?php wp_nav_menu( array('theme_location' => 'Меню', 'depth' => '2', 'container' => '' )); ?></nav>
<?php //if ($_SERVER['REQUEST_URI'] == '/') { ?>
<!--		<div class="slider">-->
<!--		--><?php //$sliders = get_post_meta(2, 'ar_slider', true); if($sliders != '') { ?>
<!--			<div class="cycle-slideshow" data-cycle-slides="> div" data-cycle-timeout="7000" data-cycle-speed="200" data-cycle-pause-on-hover="true" data-cycle-prev="#slider_prev" data-cycle-next="#slider_next" data-cycle-pager=".slider_nav" data-cycle-pager-template="">-->
<!--			--><?php //foreach ( (array) $sliders as $key => $slider ) {
//				$image = $url = '';
//				if ( isset( $slider['image'] ) )
//					$image = esc_html( $slider['image'] );
//				if ( isset( $slider['url'] ) )
//					$url = esc_html( $slider['url'] );
//				if ( isset( $slider['text2'] ) )
//					$text2 = $slider['text2'];
//
//			 ?>
<!--				<div class="slide">-->
<!--					--><?php //if($text2 != '') { ?><!--<div class="text">--><?php //echo $text2; ?><!--</div>--><?php //} ?>
<!--					--><?php //if($url != '') { ?><!--<a href="--><?php //echo $url; ?><!--">--><?php //} ?><!----><?php //if($image != '') { ?><!--<img src="--><?php //echo $image; ?><!--" />--><?php //} ?><!----><?php //if($url != '') { ?><!--</a/>--><?php //} ?>
<!--				</div>-->
<!--			--><?php //} ?>
<!--			</div>-->
<!--			<a id="slider_prev" href="#"></a>-->
<!--			<a id="slider_next" href="#"></a>-->
<!--			<div class="slider_nav slider_nav_icon">-->
<!--			--><?php //foreach ( (array) $sliders as $key => $slider ) { ?>
<!--				<span></span>-->
<!--			--><?php //} ?>
<!--			</div>-->
<!--			<div class="slider_nav slider_nav_text">-->
<!--			--><?php //$i=1; foreach ( (array) $sliders as $key => $slider ) {
//				$text1 = '';
//				if ( isset( $slider['text1'] ) )
//					$text1 = esc_html( $slider['text1'] );
//				if ( isset( $slider['url'] ) )
//					$url = esc_html( $slider['url'] );
//			 ?>
<!--				<a href="--><?php //echo $url; ?><!--"><span>--><?php //echo $i; $i++; ?><!--</span> --><?php //echo $text1; ?><!--</a>-->
<!--			--><?php //} ?>
<!--			</div>-->
<!--		--><?php //} ?>
<!--		</div>-->
<?php //} ?>
<!--		<div class="top_cats_t"><noindex>Наша продукция</noindex></div>-->
<!--		<div class="top_cats">-->
<!--			<a class="top_cats_list top_cats1" href="--><?php //echo get_page_link(72); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Полимерные полы</span></a>-->
<!--			<a class="top_cats_list top_cats4" href="--><?php //echo get_page_link(164); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Упрочненные полы</span></a>-->
<!--			<a class="top_cats_list top_cats5" href="--><?php //echo get_page_link(153); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Бетонные полы</span></a>-->
<!--			<a class="top_cats_list top_cats3" href="--><?php //echo get_page_link(69); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Гидроизоляция</span></a>-->
<!--			<a class="top_cats_list top_cats7" href="--><?php //echo get_page_link(155); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Бесшовная кровля</span></a>-->
<!--			<a class="top_cats_list top_cats6" href="--><?php //echo get_page_link(62); ?><!--"><span class="top_cats_i"></span><span class="top_cats_t">Спортивные покрытия</span></a>-->
<!--			<div class="clear"></div>-->
<!--		</div>-->
	</header>
<div class="headerscrollitem"></div>
        <?php if ($_SERVER['REQUEST_URI'] == '/') { ?>
            <div class="slider">
                <?php $sliders = get_post_meta(2, 'ar_slider', true); if($sliders != '') { ?>
                    <div class="cycle-slideshow" data-cycle-slides="> div" data-cycle-timeout="7000" data-cycle-speed="200" data-cycle-pause-on-hover="true" data-cycle-prev="#slider_prev" data-cycle-next="#slider_next" data-cycle-pager=".slider_nav" data-cycle-pager-template="">
                        <?php foreach ( (array) $sliders as $key => $slider ) {
                            $image = $url = '';
                            if ( isset( $slider['image'] ) )
                                $image = esc_html( $slider['image'] );
                            if ( isset( $slider['url'] ) )
                                $url = esc_html( $slider['url'] );
                            if ( isset( $slider['text2'] ) )
                                $text2 = $slider['text2'];

                            ?>
                            <div class="slide">
                                <?php if($text2 != '') { ?><div class="text"><?php echo $text2; ?></div><?php } ?>
                                <?php if($url != '') { ?><a href="<?php echo $url; ?>"><?php } ?><?php if($image != '') { ?><img src="<?php echo $image; ?>" /><?php } ?><?php if($url != '') { ?></a/><?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <a id="slider_prev" href="#"></a>
                    <a id="slider_next" href="#"></a>
                    <div class="slider_nav slider_nav_icon">
                        <?php foreach ( (array) $sliders as $key => $slider ) { ?>
                            <span></span>
                        <?php } ?>
                    </div>
                    <div class="slider_nav slider_nav_text">
                        <?php $i=1; foreach ( (array) $sliders as $key => $slider ) {
                            $text1 = '';
                            if ( isset( $slider['text1'] ) )
                                $text1 = esc_html( $slider['text1'] );
                            if ( isset( $slider['url'] ) )
                                $url = esc_html( $slider['url'] );
                            ?>
                            <a href="<?php echo $url; ?>"><span><?php echo $i; $i++; ?></span> <?php echo $text1; ?></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
