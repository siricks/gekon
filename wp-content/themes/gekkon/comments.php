<?php
	if (isset($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	
	if ( post_password_required() ) { ?>
		<p class="nocomments">Это сообщение защищено паролем. Введите пароль, чтобы посмотреть комментарии.</p> 
	<?php
		return;
	}
?>

<section id="comments">

<?php if ( have_comments() ) : ?>

	<div class="comments_t"><?php comments_number(__('0 комментариев'), __('1 комментарий'), __('% комментариев'));?></div>

	<ul class="comments_list">
	<?php wp_list_comments('type=comment&callback=raft_comment'); ?>
	</ul>

	<div class="comm_navi">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

<?php else : ?>
	<?php if ('open' == $post->comment_status) : ?>
	<?php else : ?>
	<p class="nocomments">Комментарии запрещены.</p>
	<?php endif; ?>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>
<div id="respond">
	<div class="respond_t">Добавить комментарий</div>
	<div id="cancel-comment-reply"> 
		<p><i><?php cancel_comment_reply_link() ?></i></p>
	</div> 
	
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p><?php printf(__('Вы должны <a href="%s">авторизоваться</a> для отправки комментария.'), get_option('siteurl') . '/wp-login.php?redirect_to=' . urlencode(get_permalink())); ?></p>
	<?php else : ?>
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
	
		<?php if ( $user_ID ) : ?>
		
		<p><?php printf(__('Вы вошли как <a href=\"%1$s\">%2$s</a>.'), get_option('siteurl') . '/wp-admin/profile.php', $user_identity); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Выйти из этого аккаунта">Выйти &raquo;</a></p>
		
		<?php else : ?>
		
		<div class="respond_inp">
			<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" <?php if ($req) echo "aria-required='true'"; ?> />
			<label for="author">Имя <?php if ($req) _e("(обязательно)"); ?></label>
		</div>
		
		<div class="respond_inp">
			<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" <?php if ($req) echo "aria-required='true'"; ?> />
			<label for="email">E-mail (не публикуется) <?php if ($req) _e("(обязательно)"); ?></label>
		</div>
		
		<div class="respond_inp">
			<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" />
			<label for="url">Web-сайт</label>
		</div>
		
		<?php endif; ?>
		
		<div class="respond_inp"><textarea name="comment" id="comment" cols="60%" rows="5"></textarea></div>
		
		<div class="respond_sub"><input name="submit" type="submit" id="submit" value="Отправить" /></div>

		<?php comment_id_fields(); ?> 
		<?php do_action('comment_form', $post->ID); ?>
	
	</form>
	<?php endif; ?>
</div>
<?php endif; ?>

</section>