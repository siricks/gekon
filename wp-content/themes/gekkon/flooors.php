<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/ustrojstvo-sportivnyh-ploshhadok/">
                    <img src="/wp-content/uploads/2015/04/besshov-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/ustrojstvo-sportivnyh-ploshhadok/">Бесшовное покрытие из резиновой крошки</a></div></td>
        <td>
            <div><a href="/rezinovaya-plitka/">
                    <img src="/wp-content/uploads/2015/04/rezinplit-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href=" /rezinovaya-plitka/">Резиновая плитка</a></div></td>
        <td>
            <div><a href="/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">
                    <img src="/wp-content/uploads/2015/04/rylonpokr-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">Резиновое покрытие рулонного типа</a></div></td>
        <td>
            <div><a href="/chto-takoe-nalivnoe-rezinovoe-pokrytie/">
                    <img src="/wp-content/uploads/2015/04/nalivpol-150x150.png" alt="" />
                </a></div>
            <div class="linkPol"><a href="/chto-takoe-nalivnoe-rezinovoe-pokrytie/">Наливные полы</a></div></td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">Резиновые покрытия по типу объекта:</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/sportivnye-ploshhadki-2/">
                    <img src="/wp-content/uploads/2015/04/sportivniyeplosh-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/sportivnye-ploshhadki-2/">Спортивные площадки</a></div></td>
        <td>
            <div><a href="/detploshad/">
                    <img src="/wp-content/uploads/2015/04/detplosh-150x150.jpg" alt="" /> </a></div>
            <div class="linkPol"><a href="/detploshad/">Детские площадки</a></div></td>
        <td>
            <div><a href="https://gekkonsystem.ru/pokrytiya-dlya-fitnes-centrov/">
                    <img src="/wp-content/uploads/2015/04/fitnes-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="https://gekkonsystem.ru/pokrytiya-dlya-fitnes-centrov/">Фитнес центры</a></div></td>
        <td>
            <div><a href="/pokritiya-dlya-lestnic/">
                    <img src="/wp-content/uploads/2015/04/travmoopasn-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/pokritiya-dlya-lestnic/">Уличные травмоопастные места</a></div></td>
    </tr>
    </tbody>
</table>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/pokrytiya-dlya-vystavochnyx-pomeshhenij/">
                    <img src="/wp-content/uploads/2015/04/vystavoch-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/pokrytiya-dlya-vystavochnyx-pomeshhenij/">Выставочные залы</a></div></td>
        <td>
            <div><a href="/wp-content/uploads/2015/04/mestaobchpolz.jpg">
                    <img src="/wp-content/uploads/2015/04/mestaobchpolz-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="#">Места общего пользования</a></div></td>
        <td>
            <div><a href="/wp-content/uploads/2015/04/korty.jpg">
                    <img src="/wp-content/uploads/2015/04/korty-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="#">Корты, футбольные поля</a></div></td>
        <td>
            <div><a href="/basseyn-2/">
                    <img src="/wp-content/uploads/2015/04/basseyn-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/basseyn-2/">Бассейн</a></div></td>
    </tr>
    </tbody>
</table>

<div class="div-page-menu-new">
    <ul id="pagetabs" class="tabs-22">
        <li><a href="#" class="page-button-new">ОПИСАНИЕ</a></li>
        <li><a href="#" class="page-button-new">ПРЕИМУЩЕСТВА ПОЛИМЕРНЫХ КРОВЕЛЬ:</a></li>
        <li><a href="#" class="page-button-new">ЦЕНЫ НА БЕСШОВНЫЕ КРОВЛИ:</a></li>
        <li><a href="#" class="page-button-new">МАТЕРИАЛЫ</a></li>
    </ul>
</div>
<div id="pagepanes" class="panes" style="">
    <div class="desc-desc">
        <div class="title_color titlePol">Коротко о резиновых полах</div>
        Все виды резиновых покрытий объединяет общий состав материалов. Везде используется резиновая крошка, пигмент и полиуретановое связующее. Однако, несмотря на одинаковые составляющие, каждое покрытие используется в определенных целях, имеют различия при монтаже.

        Так, например, для бега и игры в активные виды спорта подходит бесшовное резиновое покрытие, так как оно укладывается толщиной 10 мм. и не является слишком мягким. Тогда как для детских площадок подойдет резиновая плитка, из-за высокой толщины, она имеет хорошую амортизацию для безопасности детей, да еще и является яркой, что отлично подходит для детей.

        Но у всех материалов есть общие характеристики, благодаря которым они и активно используются. Это в первую очередь, безопасность, которую обеспечивает хорошая амортизация и антискользящие свойства, долговечность и неприхотливость, что позволяет активно эксплуатировать их на улице.

        Резиновая плитка и рулонные резиновые покрытия продаются в готовом виде, поэтому они более просты в укладке, возможно даже самостоятельное устройство. Бесшовное резиновое покрытие поставляется отдельно. Все компоненты смешиваются на объекте, из-за его устройство более трудоемко и требует определенных навыков. Наливные резиновые покрытия, являются одним из наиболее сложных напольных покрытий, его устройство является многоступенчатым и длительным, поэтому неспециалистам практически невозможно аккуратно выполнить такое покрытие.
        <div class="title_color titlePol">Бесшовное покрытие из резиновой крошки</div>
        Является единственным вариантов реально спортивного уличного покрытия. Монтаж происходит непосредственно на объекте. Смешивая перед укладкой полиуретановое связующее, резиновую крошку и пигмент. Благодаря ручному устройству покрытия, возможно, варьировать толщину покрытия, в зависимости от нужд заказчика. Данное покрытие получить бесшовным и монолитным. Как правило, поверх наноситься разметка.

        Основным составляющим является полиуретановое связующее, мы советуем не покупать дешевое сырье, так как оно плохо выдерживает различный температурные циклы (зима – лето) и начнет сыпаться после первых оттепелей.

        Также при устройстве бесшовных покрытий сверху может быть положена цветная EPDM крошка. EPDM - это каучуковая яркая крошка, которая укладывается для придания более красивого внешнего вида. Ее часто можно видеть на детских площадках. В случае укладки EPDM, снизу кладется обычная черная крошка без пигмента, а поверх нее в тонкий слой кладется каучуковая крошка. Это делается из-за того что EPDM имеет очень высокую цену.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="/besshovnoe-pokrytie-iz-rezinovoj-kroshki-texnologiya/">Технология устройства</a></div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5"><a href="https://gekkonsystem.ru/ustrojstvo-sportivnyh-ploshhadok/">Цена</a></div>
            </div>
        </div>
        <div class="title_color titlePol">Резиновая плитка</div>
        – активно используется для детских площадок, большая толщина, простота в укладке и демонтаже, ну и конечно безопасность, делают плитку очень популярным материалом.

        Однако последнее время резиновая плитка все чаше становиться объектом применения не только на детских и спортивных площадках, но и как замена тротуарной плитки на участках, и придомовых территориях. Так как она обладает необходимой износостойкостью, на ряду с антискользящими свойствами и безопасностью при падениях
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4">Технология устройства</div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5"><a href="https://gekkonsystem.ru/rezinovaya-plitka/">Цена</a></div>
            </div>
        </div>
        <div class="title_color titlePol">Резиновое покрытие рулонного типа</div>
        Все фитнес центры всегда используют именно данный вид покрытия. Рулонные материалы, обычно, укладываются тонким слоем на хорошо подготовленное основание, поэтому чаще всего эксплуатируются в закрытых помещениях. Нет другого покрытия, которое безболезненно выдержит падение 150 кг. штанги. Также в отличие от бесшовной резины, рулонная легко демонтируется и монтируется на самом последнем этапе строительства. Однако использование толстых покрытий (от 20 мм.) позволяется быстро уложить резиновые покрытия рулонного типа на уличных площадках.

        Сейчас стало все более популярным использование рулонных резиновых покрытий на складах, производствах и просто местах общего пользования. Так как даже полиуретановые полы не обладают такой эластичностью, амортизацией и вибропоглощением, как резиновые покрытия.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4"><a href="https://gekkonsystem.ru/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">Технология устройства</a></div>
            </div>
            <div class="col3">
                <div class="title_color title_color2"><a href="https://gekkonsystem.ru/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">Материалы</a></div>
            </div>
            <div class="col3">
                <div class="title_color title_color5"><a href="https://gekkonsystem.ru/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">Цена</a></div>
            </div>
        </div>
        <div class="title_color titlePol">Наливные полы</div>
        Технология наливных полов на спортивных объектах отличается от всех вышеизложенных материалов. Наливные спортивные полы – это симбиоз наливных полимерных полов и резиновых покрытий.

        Технология заключается в устройстве подожки из бесшовного резинового покрытия, а сверху устройства модифицированного наливного пола. Итоговое покрытие получается идеально гладким как полимерный пол, но с ощутимо повышенными амортизационными характеристиками, за счет подложки из резиновой крошки.

        Такие полы можно использовать где угодно, однако за счет высокой цены, как правило они используются только в специальных обязательных местах, таких как мини-футбольные поля, закрытые спортивные сооружения, бадминтонные корты и т.д.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4">Технология устройства</div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="title_color titlePol titlePol2">Преимущество резиновых покрытий:</div>
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color2"><a href="https://gekkonsystem.ru/ustrojstvo-sportivnyh-ploshhadok/">Бесшовное покрытие из резиновой крошки</a></div>
                <div class="col_pad">

                    - Долговечность
                    - Простота в эксплуатации (ремонте)
                    - Неприхотливость
                    - Возможность контролировать толщину слоя
                    - Безопасность

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color3"><a href="https://gekkonsystem.ru/rezinovaya-plitka/">Резиновая плитка</a></div>
                <div class="col_pad">

                    - Максимальная безопасность
                    - Возможность монтажа в любую погоду
                    - Возможность демонтажа и переноса
                    - Различные дизайнерские решения

                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color4"><a href="https://gekkonsystem.ru/rulonnoe-rezinovoe-pokrytie-chto-eto-takoe/">Резиновое покрытие рулонного типа</a></div>
                <div class="col_pad">

                    - Безопасность
                    - Простота в укладке
                    - Возможность демонтажа
                    - Покрытие для внутренних помещений

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color5"><a href="/">Наливные полы</a></div>
                <div class="col_pad">

                    - профессиональное покрытие для многих видов спорта
                    - Идеально гладкое
                    - Мягкая амортизация

                </div>
            </div>
        </div>
    </div>
    <div class="prices">
        <div class="title_color titlePol">Цены на резиновые покрытия</div>
        Если говорить о материалах, то цена на резиновые покрытия зависят от двух важных моментов:

        1) Толщина покрытия

        2) Тип покрытия

        Более подробные цены на материалы смотрите в каждом разделе отдельно.

        Более подробно остановимся здесь, чтобы поговорить о ценах на работу.

        Стоимость работ на устройство бесшовных резиновых покрытий, рулонных и плитки зависит от одного важного момента - <span style="text-decoration: underline;">качество подготовки основания</span>. Лучшим основанием будет являться твердое основание, такое как бетон или асфальт. В случае если такого основание нет, а делать его нет возможности, то подойдет уплотненный отсев, ПГС и прочее провибрированное основание.

        Цены предоставленные ниже рассчитаны исходя из подготовленного основания, толщиной 10-15 мм., терракотового (красного) цвета
        <table width="100%">
            <tbody>
            <tr>
                <td width="322"></td>
                <td width="76"><strong>от 100 кв.м.</strong></td>
                <td width="76"><strong>от 250 кв.м.</strong></td>
                <td width="66"><strong>от 500 кв.м.</strong></td>
                <td width="76"><strong>от 1000 кв.м</strong></td>
            </tr>
            <tr>
                <td width="322"><strong>Бесшовное покрытие из резиновой крошки</strong></td>
                <td width="76"></td>
                <td width="76"></td>
                <td width="66"></td>
                <td width="76"></td>
            </tr>
            <tr>
                <td width="322">Материалы на 1 кв.м.</td>
                <td width="76">990</td>
                <td width="76">800</td>
                <td width="66">700</td>
                <td width="76">650</td>
            </tr>
            <tr>
                <td width="322">Цена 1 кв.м. под ключ</td>
                <td width="76">1500</td>
                <td width="76">1200</td>
                <td width="66">1000</td>
                <td width="76">900</td>
            </tr>
            <tr>
                <td width="322"><strong>Резиновая плитка</strong></td>
                <td width="76"></td>
                <td width="76"></td>
                <td width="66"></td>
                <td width="76"></td>
            </tr>
            <tr>
                <td width="322">Материалы на 1 кв.м.</td>
                <td width="76">1130</td>
                <td width="76">1110</td>
                <td width="66">1090</td>
                <td width="76">1060</td>
            </tr>
            <tr>
                <td width="322">Цена 1 кв.м. под ключ</td>
                <td width="76"> 1430</td>
                <td width="76">1350</td>
                <td width="66"> 1290</td>
                <td width="76">1180</td>
            </tr>
            <tr>
                <td width="322"><strong>Резиновое покрытие рулонного типа</strong></td>
                <td width="76"></td>
                <td width="76"></td>
                <td width="66"></td>
                <td width="76"></td>
            </tr>
            <tr>
                <td width="322">Материалы на 1 кв.м.</td>
                <td width="76">1050</td>
                <td width="76">1010</td>
                <td width="66">980</td>
                <td width="76">920</td>
            </tr>
            <tr>
                <td width="322">Цена 1 кв.м. под ключ</td>
                <td width="76">1400</td>
                <td width="76">1300</td>
                <td width="66">1250</td>
                <td width="76">1190</td>
            </tr>
            <tr>
                <td width="322"><strong>Наливные полы</strong></td>
                <td width="76"></td>
                <td width="76"></td>
                <td width="66"></td>
                <td width="76"></td>
            </tr>
            <tr>
                <td width="322">Материалы на 1 кв.м.</td>
                <td width="76">1950</td>
                <td width="76">1890</td>
                <td width="66">1850</td>
                <td width="76">1800</td>
            </tr>
            <tr>
                <td width="322">Цена 1 кв.м. под ключ</td>
                <td width="76">2900</td>
                <td width="76">2780</td>
                <td width="66">2650</td>
                <td width="76">2500</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="materials">
        <div class="title_color titlePol">Основные материалы</div>
        &nbsp;
        <table class="table_null" border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="220"><img src="/wp-content/uploads/2015/04/mc.png" alt="" width="200" /></td>
                <td valign="top"><strong>Геккон МС</strong>. Полиуретановое связующее для резиновой крошки. Цена от 190 руб. за кг. Применяется при укладке спортивных площадок из резиновой крошки, путем смешивания клея с крошкой и красящим пигментом. Также применяется как связующее при устройстве методом горячего формования резиновых плиток и рулонов.                                                                                                                  Обладает отличной адгезией к любым типом материалов и высокими характеристиками эластичности. Выдерживает температурные перепады.           При правильной укладке гарантия дается на 3 года- реальный срок службы 10-15лет.</td>
            </tr>
            <tr>
                <td width="220"><a href="https://gekkonsystem.ru/wp-content/uploads/2015/04/phpcR3.jpg"><img class="alignnone  wp-image-1773" src="https://gekkonsystem.ru/wp-content/uploads/2015/04/phpcR3.jpg" alt="phpcR" width="200" height="113" /></a></td>
                <td valign="top"><strong>Резиновая крошка разных фракций. </strong>Цена от 17 руб. за кг.<strong> </strong>Чистая крошка без металлических и тканевых примесей. Важно!  Если вы используете при укладке крошку с добавлением примесей, то на 1 кв.м. будет уходить значительно больше связующего и пигмента, что при его высокой стоимости будет экономически нецелесообразно.</td>
            </tr>
            <tr>
                <td width="220"><a href="https://gekkonsystem.ru/wp-content/uploads/2015/04/17_135.png"><img class="alignnone size-full wp-image-1775" src="https://gekkonsystem.ru/wp-content/uploads/2015/04/17_135.png" alt="17_135" width="200" height="113" /></a></td>
                <td valign="top"> <strong>Пигмент. </strong>от 86 руб. за кг. Для бесшовных покрытий существуют пигменты красного, синего и зеленого цвета. Расход пигмента составляет около 25 кг на 100 кв.м. при толщине покрытия 10 мм.</td>
            </tr>
            <tr>
                <td width="220"></td>
                <td valign="top"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
[contact-form-7 id="2487" title="заказать обратный звонок"]
<script> jQuery(document).ready(function () {
        jQuery("ul.tabs-22").tabs("div.panes > div");
    });
</script>
Резиновые покрытия для полов в современном мире можно используются повсеместно . Обладая уникальными характеристиками резиновые покрытия безальтернативно являются единственным типом покрытия используемых на спортивных и детских уличных площадках, полов для практически всех объектов спортивного назначения, в т.ч. фитнес центры и тренажерные залы, поля для мини-футбола, тенниса и т.д. Однако спортивными и детскими объектами использование не заканчивается. Также резину можно встретить в гаражах, производственных помещениях, складах и выставочных помещениях.

Основные причины по которым резина стала настолько популярна это ее:

- износостойкость, которая позволяет быть, по сути, единственным покрытием используемом на улице.

- амортизационные характеристика, следствием чего является безопасность. Также благодаря амортизации резиновые покрытия укладываются под станками, для вибро и шумопоглащения

- водонепроницаемость и противоскользящие качества, что также является необходимым условиям для безопасности.

- простота укладки, что позволяет быть достаточно экономичным решением.



