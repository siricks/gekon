<?php get_header(); ?>

	<section id="content">
		<section id="posts">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="breadc"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></div>
	
			<article class="post prod">
				<div class="title">
					<?php $prod_cat = '';
					$terms = get_the_terms( $post->ID, 'prod_cat' );
					if ( $terms && ! is_wp_error( $terms ) ) : 
						$term_title = array(); 
						foreach ( $terms as $term ) { 
							$term_title[] = $term->name;
						} 
						$prod_cat = join( ", ", $term_title );
					endif; 
					echo $prod_cat;
					?>
				</div>
				<div class="prod_images">
					<?php echo raft_post_img_wp($post->ID, 2, 1, 'img260', '<div class="prod_image">', '</div>'); ?>
					<?php $images =  get_post_meta(get_the_ID(), 'ar_images', true); if($images != '') { ?>
						<?php foreach ($images as $image_id => $image) { ?>
							<?php echo raft_post_img_wp($post->ID, 2, 1, '', '<div class="prod_imgs_list">', '</div>', $image_id); ?>
						<?php } ?>
						<div class="clear"></div>
					<?php } ?>
				</div>
				<div class="prod_r prod_parent">
					<div class="prod_parent_t"><h1><?php $title = the_title( '', '', false ); if (get_field('h1')) {the_field('h1');} else {echo $title;} ?></h1></div>
					<div class="prod_parent_u"><?php the_permalink() ?></div>
					<div class="prod_params">
					<?php
					$fields_group = 701;
					$fields = get_field_objects(); $fields_sort=array();
					if( $fields ) {
						echo '<div class="prod_params_t">Характеристики:</div>';
						foreach( $fields as $field_name => $field ) {
							if ( (int)$field['field_group']==$fields_group && $field['value'] != '' )  {
								if (is_array($field['value'])) {
									$field_value = implode(", ", $field['value']);
								} else {
									$field_value = $field['value'];
								}
								$fields_sort[ (int)$field['order_no'] ] = '<div class="prod_param"><div class="prod_param_l">'.$field["label"].'</div> <div class="prod_param_r">'.$field_value.'</div><div class="clear"></div></div>';
							}
						}
						ksort($fields_sort);
						foreach ( $fields_sort as $output ) { echo $output; }
					} ?>
					</div>
					<?php echo raft_custum_field($post->ID, 'ar_price_min', 0, '<div class="prod_p">Цена: <span>', '</span></div>'); ?>
					<div class="prod_add"><a class="modal_fast" href="#fast_cart">Купить</a></div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
				<div class="entry entry_pad">
					<?php the_content() ?>
					<div class="seo_text"><? the_field('seo_text');?></div>
					<div class="clear"></div>
				</div>
			</article>
	
		<?php endwhile; endif; ?>
		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</section>

<?php get_footer(); ?>