<?php get_header(); ?>

	<section id="content">
		<section id="posts">
			<div class="breadc"><?php wp_reset_query(); if(function_exists('bcn_display')) { bcn_display(); } ?></div>
		<div class="post">

<?php if (is_category(24)) { ?>


		<div class="nash_ob_l">
			<div class="title"><noindex>Наливные полы</noindex></div>
			<?php $news = new WP_query(); $news->query('showposts=100&cat=25'); ?>
			<?php while ($news->have_posts()) : $news->the_post(); ?>
			<div class="nash_ob">
				<div class="nash_ob_img"><?php echo raft_post_img_wp($post->ID, 1, 1); ?></div>
				<div class="nash_ob_te">
					<div class="nash_ob_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
					<div class="nash_ob_e"><noindex><?php echo raft_announce($post->post_content, 100); ?></noindex></div>
				</div>
				<div class="clear"></div>
			</div>
			<?php endwhile; ?>
		</div>
		<div class="nash_ob_r">
			<div class="title"><noindex>Гидроизоляция</noindex></div>
			<?php $news = new WP_query(); $news->query('showposts=100&cat=26'); ?>
			<?php while ($news->have_posts()) : $news->the_post(); ?>
			<div class="nash_ob">
				<div class="nash_ob_img"><?php echo raft_post_img_wp($post->ID, 1, 1); ?></div>
				<div class="nash_ob_te">
					<div class="nash_ob_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
					<div class="nash_ob_e"><noindex><?php echo raft_announce($post->post_content, 100); ?></noindex></div>
				</div>
				<div class="clear"></div>
			</div>
			<?php endwhile; ?>
		</div>
		<div class="clear"></div>

<?php } else { ?>

		<?php if (have_posts()) : ?>
	
		<?php $post = $posts[0]; ?>
		<?php if (is_category()) { ?>
			<h1><?php printf(__('%s'), single_cat_title('', false)); ?></h1>
		<?php } elseif( is_tag() ) { ?>
			<h1><?php printf(__('%s'), single_tag_title('', false) ); ?></h1>
		<?php } elseif (is_day()) { ?>
			<h1><?php printf(_c('%s'), get_the_time(__('d.m.Y'))); ?></h1>
		<?php } elseif (is_month()) { ?>
			<h1><?php printf(_c('%s'), get_the_time(__('F Y'))); ?></h1>
		<?php } elseif (is_year()) { ?>
			<h1><?php printf(_c('%s'), get_the_time(__('Y'))); ?></h1>
		<?php } elseif (is_author()) { ?>
			<h1>Архив автора</h1>
		<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h1>Архив блога</h1>
		<?php } elseif (is_search()) { ?>
			<h1>Результаты поиска</h1>
		<?php } ?>

		<?php while (have_posts()) : the_post(); ?>
	
			<div class="post_list">
				<?php echo raft_post_img_wp($post->ID, 1, 1, '', '<div class="post_list_img">', '</div>'); ?>
				<div class="post_list_r">
					<div class="post_list_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
					<div class="post_list_d"><noindex><?php the_time('d/m/Y') ?></noindex></div>
					<div class="post_list_e"><noindex><?php echo raft_announce($post->post_content, 300); ?></noindex></div>
				</div>
				<div class="clear"></div>
			</div>
	
		<?php endwhile; ?>
	
			<div class="p_nav"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
	
		<?php else : ?>
	
			<h2 class="center">Не найдено</h2>
			<p class="center">Извините, но вы ищете то, чего здесь нет.</p>
	
		<?php endif; ?>
		</div>

<?php } ?>

		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
		<?php include (TEMPLATEPATH . '/primeri.php'); ?>
	</section>

<?php get_footer(); ?>