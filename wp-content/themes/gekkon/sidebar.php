<style>

    #menu-levoe .sub-menu {
        display: none;
        padding-left: 5px;
    }

    #menu-levoe  li.current-menu-parent .sub-menu,
    #menu-levoe  li.current-menu-item .sub-menu{
        display: block;
    }

</style>
<aside id="sidebar">
    <nav class="sb_nav">
        <?php wp_nav_menu(array('theme_location' => 'МенюЛево', 'depth' => '2', 'container' => '')); ?>
    </nav>
    <div class="widget">
        <div class="sb_rasch"><a href="/shop/materialy/">Каталог материалов</a></div>

    </div>
    <div class="widget">
        <?php $news = new WP_query();
        $news->query('showposts=2&cat=1'); ?>
        <?php while ($news->have_posts()) : $news->the_post(); ?>
            <div class="sb_news">
                <div class="sb_news_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                <div class="sb_news_d">
                    <noindex><?php the_time('d/m/Y') ?></noindex>
                </div>
                <div class="sb_news_m"><a href="<?php the_permalink() ?>">подробнее</a></div>
                <div class="clear"></div>
                <div class="sb_news_e">
                    <noindex><?php echo raft_announce($post->post_content, 200); ?></noindex>
                </div>
            </div>
        <?php endwhile; ?>
        <div class="home_news_all"><a href="<?php echo get_category_link(1); ?>">смотреть все </a></div>
    </div>
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar')) : ?><?php endif; ?>
</aside>