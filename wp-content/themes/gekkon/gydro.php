
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/inekcionnaja-gidroizoljacija/">
                    <img src="/wp-content/uploads/2015/04/injec-150x150.jpg" alt="" /></a></div>
            <div class="linkPol"><a href="/inekcionnaja-gidroizoljacija/">Инъекционная</a></div></td>
        <td>
            <div><a href="/zachem-nuzhna-i-chto-takoe-obmazochnaya-gidroizolyaciya/">
                    <img src="/wp-content/uploads/2015/04/obmaz-150x150.jpg" alt="" /></a></div>
            <div class="linkPol"><a href="/zachem-nuzhna-i-chto-takoe-obmazochnaya-gidroizolyaciya/">Обмазочная</a></div></td>
        <td>
            <div><a href="/chto-my-nazyvaem-pronikayushhej-gidroizolyaciej/">
                    <img src="/wp-content/uploads/2015/04/pronik-150x150.jpg" alt="" /></a></div>
            <div class="linkPol"><a href="/chto-my-nazyvaem-pronikayushhej-gidroizolyaciej/">Проникающая</a></div></td>
        <td>
            <div><a href="/gidroizoljacija/napyljaemaja/">
                    <img src="/wp-content/uploads/2015/04/napyl-150x150.jpg" alt="" /></a></div>
            <div class="linkPol"><a href="/gidroizoljacija/napyljaemaja/">Напыляемая</a></div></td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">По типу проблемы</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/gidroizolyaciya-primykanij/">
                    <img src="/wp-content/uploads/2015/04/primik-150x150.jpg" alt="" /></a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-primykanij/">Примыкания</a></div></td>
        <td>
            <div><a href="https://gekkonsystem.ru/gidroizoljacija/betonnye-shvy-i-treshhiny/">
                </a><a href="/gidroizolyacia_shvov_betonirovaniya_treshin/"><img class="alignnone" src="/wp-content/uploads/2015/04/shvybet-150x150.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="/gidroizolyacia_shvov_betonirovaniya_treshin/">Швы бетонирования и трещины</a></div></td>
        <td>
            <div><a href="https://gekkonsystem.ru/gidroizoljacija/deformacionnye-shvy/">
                </a><a href="/gidroizolyacia_deformacionnih_shvov/"><img class="alignnone" src="/wp-content/uploads/2015/04/deform-150x150.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="/gidroizolyacia_deformacionnih_shvov/">Деформационные швы</a></div></td>
        <td>
            <div><a href="https://gekkonsystem.ru/gidroizolyaciya_vvodov_kommunikacij/">
                </a><a href="https://gekkonsystem.ru/gidroizolyaciya_vvodov_kommunikacij/"><img class="alignnone" src="/wp-content/uploads/2015/04/vvody-150x150.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="gidroizolyaciya_vvodov_kommunikacij">Места вводов коммуникаций</a></div></td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">По типу объекта</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/gidroizolyaciya-parkingov/">
                    <img class="alignnone" src="/wp-content/uploads/2015/04/po_tipu_parking2-150x150.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-parkingov/">Паркинг</a></div></td>
        <td>
            <div><a href="/gidroizolyaciya-podvala/">
                    <img src="/wp-content/uploads/2015/04/podval-150x150.jpg" alt="" /> </a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-podvala/">Подвал</a></div></td>
        <td>
            <div><a href="https://gekkonsystem.ru/gidroizolyaciya-liftovogo-priyamka/">
                    <img src="/wp-content/uploads/2015/04/priyam-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="https://gekkonsystem.ru/gidroizolyaciya-liftovogo-priyamka/">Приямок</a></div></td>
        <td>
            <div><a href="/gidroizolyaciya-tonnelya/">
                    <img src="/wp-content/uploads/2015/04/tunnel-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-tonnelya/">Тоннель</a></div></td>
    </tr>
    <tr>
        <td>
            <div><a href="/wp-content/uploads/2015/04/most.jpg">
                    <img src="/wp-content/uploads/2015/04/most-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="#">Мостовые сооружения</a></div></td>
        <td style="text-align: center;">
            <div><a href="/gidroizolyaciya-krovli/">
                    <img src="/wp-content/uploads/2015/04/krovl-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-krovli/">Кровля</a></div></td>
        <td>
            <div><a href="/gidroizoljacija/bassejn/">
                    <img src="/wp-content/uploads/2015/04/basseyn-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/gidroizoljacija/bassejn/">Бассейн</a></div></td>
        <td>
            <div><a href="/gidroizolyaciya-fundamenta/">
                    <img src="/wp-content/uploads/2015/04/fundament-150x150.jpg" alt="" />
                </a></div>
            <div class="linkPol"><a href="/gidroizolyaciya-fundamenta/">Фундамент</a></div></td>
    </tr>
    <tr>
        <td>
            <div><a href="/gidroizoljacija/poly/">
                    <img src="/wp-content/uploads/2015/04/Vidy_tonkosloynoye_pokritiye1-150x150.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="/gidroizoljacija/poly/">Полы</a></div></td>
        <td>
            <div><a href="/gidroizoljacija/poly/">
                    <img src="/wp-content/uploads/2016/11/Бетонный-пол.jpg" alt="" width="150" height="150" /></a></div>
            <div class="linkPol"><a href="/gidroizoljacija/betonnye-shvy-i-treshhiny/">Бетонные швы и трещины</a></div></td>
    </tr>
    </tbody>
</table>
<div class="div-page-menu-new">
    <ul id="pagetabs" class="tabs-22">
        <li><a href="#" class="page-button-new">ОПИСАНИЕ</a></li>
        <li><a href="#" class="page-button-new">ПРЕИМУЩЕСТВА БЕТОННЫХ ПОЛОВ:</a></li>
        <li><a href="#" class="page-button-new">ЦЕНЫ НА БЕТОННЫЕ ПОЛЫ:</a></li>
        <li><a href="#" class="page-button-new">МАТЕРИАЛЫ</a></li>
    </ul>
</div>
<div id="pagepanes" class="panes" style="">
<div class="desc-desc">
    <div class="title_color titlePol">Коротко о технологиях и сферах применения</div>
    <div class="title_color titlePol">Инъекционная гидроизоляция</div>
    Один из самых современных видов гидроизоляции, который представляет собой действительно надежный способ избавить от проблем с протечками. Особенностью инъектирования является то, что полимерный материал («Геккон ИП», «Геккон ИПЭ» под давлением подается в невидимые слабые места конструкции, проникает и распространяется в швах, трещинах и полостях. Под действием влаги материал расширяется и надежно запечатывает источники влаги, образуя прочную, но эластичную пробку.
    Инъекционная гидроизоляция может быть использована для герметизации:

    - непровибрированных, неплотных участков и трещин микротрещин в бетоне;
    - деформационных швов, холодных швов, герметизация соединений в ж/б конструкциях;
    - контакта «сооружение-грунт» в случае, если наружная гидроизоляция не была выполнена на стадии строительства;
    - открытых протечек.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4"><a href="https://gekkonsystem.ru/texnologiya-inekcionnoj-gidroizolyacii/">Технология устройства</a></div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Обмазочная гидроизоляция</div>
    Многослойное покрытие толщиной в несколько миллиметров. Благодаря своей дешевизне наиболее популярны материалы из битума, однако они обладают целым рядом недостатков, главные из которых – ненадежное прилегание к стене (так как по сути битум приклеивается) и неэластичность. Таким образом те, кто на стадии строительства сэкономил на внешней гидроизоляции подземной части сооружение и использовал битум, в дальнейшем с большой долей вероятности будут вынуждены переплатить за решение возникших проблем. Обмазочная гидроизоляция мастикой «Геккон М» успешно применяется:

    - на стадии строительства для защиты фундаментной плиты домов;
    - на стадии строительства для защиты подземной части стен от грунтовых вод;
    - для внутренней защиты от капиллярной влаги;
    - устройства эксплуатируемой кровли.

    В результате обработки бетонной или металлической поверхности полиуретановой мастикой «Геккон М», образуется покрытие, не позволяющее доступа воде, предохраняющее основу от деформации и разрушения. Как большой плюс стоит отметить простоту работ, ведь выполнять их могут даже сотрудники с низкой квалификацией.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4"><a href="/texnologiya-naneseniya-obmazochnoj-gidroizolyacii/">Технология устройства</a></div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Проникающая гидроизоляция</div>
    Представляет собой изоляцию поверхностей больших площадей высокотехнологичными материалами «Геккон П40». Этот вид в основном применяется:
    для внутренней гидроизоляции подвальных плит;
    для гидроизоляции дефектных стен.

    В процессе нанесения, при контакте с водой, начинается химическая реакция. В итоге получается двойной гидроизоляционный эффект: создается пленка поверх плоскости и кристаллизация пор внутри бетона.

    Проникающая гидроизоляция может быть использована как в новом строительстве, так и при ремонте. Перед нанесением необходимо механическим способом очистить поверхность от штукатурки и обезжирить, чтобы открыть доступ к капиллярной системе поверхности.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Напыляемая гидроизоляция</div>
    Используется для защиты крыш, фундаментов, подземных помещений. Этот вид гидроизоляции проявил себя как достаточно надежный, ведь финишное покрытие не имеет швов, имеет хорошую адгезию, а значит и длительный срок службы.

    Необходимо провести черту между напыляемой и жидкой гидроизоляцией. В случае с напыляемой гидроизоляцией основным материалом является полимочевина. Гидроизоляция происходит холодным распылением, двух компонентов - базисной основы и отвергающего катализатора.
    <div class="clearfix">
        <div class="col3">
            <div class="title_color title_color4">Технология устройства</div>
        </div>
        <div class="col3">
            <div class="title_color title_color2">Материалы</div>
        </div>
        <div class="col3">
            <div class="title_color title_color5">Цена</div>
        </div>
    </div>
    <div class="title_color titlePol">Коротко о сферах применения:</div>
    <strong>Гидроизоляция в паркингах и подвалах</strong>
    От целостности бетона, любого другого строительного материала, зависит прочность и долговечность любого сооружения. Наиболее подвержены проблемам гидроизоляции подземные части сооружений – паркинги и подвалы. Гидроизоляция подвалов и паркингов требует особенно тщательного подхода, поскольку эти объекты являются страдают как от капиллярной сырости, так и от прямого проникновения воды. Современные гидроизоляционные материалы и технологии их использования способны защитить от вредного воздействия влаги все узлы, важно выбрать наиболее оптимальный вариант защиты в каждом конкретном случае.

    <strong>Фундамент и наружные подземные части стен</strong>
    Гидроизоляция стен дает возможность уберечь сооружения не только от разрушающего воздействия влаги и воды, но и предупреждает угрозу появления вредоносных грибков и спор. Среди основных требований к материалам для гидроизоляции стен - неизменность их защитных качеств с течением времени, ведь срок их эксплуатации примерно должен быть равен сроку существования самого здания, механическая прочность. В качестве материала для наружной гидроизоляции стен используют мастики, но особенно эффективной в этом случае считается проникающая гидроизоляция. В отличие от мастичной, проникающая гидроизоляция способна препятствовать проникновению влаги сквозь бетон, пропитывая всю его толщу, она гидролизует весь слой бетона.

    <strong>Кровельная гидроизоляция</strong>
    Гидроизоляция крыши, имеющей плоскую форму, требует особого внимания. Материалы для гидроизоляции кровли должны быть очень прочными, способными выдержать воздействие агрессивных веществ и УФ-лучей. Этим высоким требованиям удовлетворяют полимерные мастики. Гидроизоляция кровли также осуществляется при помощи рубероида, но на стыках рубероида часто возникают протечки. Чтобы избежать этой неприятности специалисты рекомендуют использовать мастику, прочно соединяющую края рубероида. Так, гидроизоляция крыши будет гарантирована.

    <strong>Гидроизоляция бассейнов, тоннелей и каналов</strong>
    Гидроизоляция стен или гидроизоляция бассейна, конечно же, зависит от качества гидроизоляции материала, из которого они построены. А это, как правило, бетон. Для гидроизоляции бетона используются и мастичные, и порошковые, и рулонные, листовые, а также находящиеся в состоянии раствора, гидроизоляционные материалы. Какой именно материал окажется наиболее приемлемым зависит от того, где бетон будет использоваться. Например, порошковые материалы для гидроизоляции отлично зарекомендовали себя как профилактика для предотвращения проникновения грунтовых и подземных вод. Они обладают отличными гидроизоляционными свойствами, но не эластичны. А гидроизоляция бетона при помощи эластичных двухкомпонентных гидроизоляционных покрытий защищает его и от влаги, и от коррозийного влияния солей сульфатов, хлоридов.

    Наиболее оптимальным материалом для гидроизоляции бассейнов являются полимерно-битумные мембраны. Они прочны и эластичны, могут выдержать значительные деформационные воздействия, очень технологичны при укладке. С их помощью осуществляется не только гидроизоляция бассейна, но и подземных сооружений, мостов, каналов, то есть сооружений, испытывающих особенно сильное воздействие воды.

    <strong>Лифтовые приямки</strong>
    Лифтовые приямки в большинстве случаев не герметичны и часто превращаются в бассейны из-за поступления грунтовых вод. Здесь необходим целый комплекс мер, перечень которых определяется после просушивания приямка.
</div>
<div class="advantages">
<div class="title_color titlePol titlePol2">Преимущества полимерной гидроизоляции:</div>
<div class="clearfix">
    <div class="col_l">
        <div class="title_color title_color2">Инъекционная гидроизоляция</div>
        <div class="col_pad">

            - Проникает в труднодоступные места (даже те, которые не были конкретной целью инъектирования)
            - После химической реакции материал расширяется, закупоривая все ненадежные месте
            - Проникание в структуру материала
            - Эластичность и прочность

        </div>
    </div>
    <div class="col_r">
        <div class="title_color title_color3">Обмазочная гидроизоляция</div>
        <div class="col_pad">

            - Изолирующее покрытие является многослойным
            - Простота выполнения работ
            - Эластичность и устойчивость к механическим воздействиям как следствие

        </div>
    </div>
</div>
<div class="clearfix">
    <div class="col_l">
        <div class="title_color title_color4">Проникающая гидроизоляция</div>
        <div class="col_pad">

            - Повышаются прочностные и защитные качества конструкций
            - Сложнее нарушить, чем другие виды
            - Особенно эффективна в новых сооружениях

        </div>
    </div>
    <div class="col_r">
        <div class="title_color title_color5">Напыляемая гидроизоляция</div>
        <div class="col_pad">

            - Наноситься до 500 кв.м. за смену
            - Отличная адгезия у любому типу материалов
            - Бесшовная и эластичная
            - Самый лучший вариант

        </div>
    </div>
</div>
</div>
<div class="prices">
<div class="title_color titlePol">Цены на полимерную гидроизоляцию</div>
<table width="100%">
    <tbody>
    <tr>
        <td width="321">Вид работ</td>
        <td width="151">Ед. измерения объема</td>
        <td width="151">Цена. руб.</td>
    </tr>
    <tr>
        <td width="321">Примыкания пол-стена</td>
        <td width="151">п.м.</td>
        <td width="151"> 2500</td>
    </tr>
    <tr>
        <td width="321">Швы бетонирования</td>
        <td width="151">п.м.</td>
        <td width="151"> 3000</td>
    </tr>
    <tr>
        <td width="321">Деформационные швы</td>
        <td width="151">п.м.</td>
        <td width="151"> 4000</td>
    </tr>
    <tr>
        <td width="321">Места вводов коммуникаций</td>
        <td width="151">шт.</td>
        <td width="151">350</td>
    </tr>
    <tr>
        <td width="321">Активное поступление воды</td>
        <td width="151">п.м.</td>
        <td width="151">3500</td>
    </tr>
    <tr>
        <td width="321">Поврежденные участки бетона</td>
        <td width="151">кв. м.</td>
        <td width="151">800</td>
    </tr>
    <tr>
        <td width="321">Обмазочная полимерная гидроизоляция</td>
        <td width="151">кв.м.</td>
        <td width="151">950</td>
    </tr>
    <tr>
        <td width="321">Полимерная гидроизоляция пола</td>
        <td width="151">кв.м.</td>
        <td width="151">450</td>
    </tr>
    </tbody>
</table>
</div>
<div class="materials">
<div class="title_color">Основные материалы</div>
<table class="table_null" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="220"><img src="/wp-content/uploads/2015/04/ip.png" alt="" width="200" /></td>
        <td valign="top">Геккон ИП.</td>
    </tr>
    <tr>
        <td width="220"><img src="/wp-content/uploads/2015/04/ipe.png" alt="" width="200" /></td>
        <td valign="top">Геккон ИПЭ.</td>
    </tr>
    <tr>
        <td width="220"><img src="/wp-content/uploads/2015/04/mc.png" alt="" width="200" /></td>
        <td valign="top">ГЕККОН МС</td>
    </tr>
    </tbody>
</table>
</div>
</div>
[contact-form-7 id="2487" title="заказать обратный звонок"]
<script> jQuery(document).ready(function () {
        jQuery("ul.tabs-22").tabs("div.panes > div");
    });
</script>

<strong>Гидроизоляция</strong> (от гидро... и изоляция) - защита строительных конструкций, зданий и сооружений от проникновения воды (антифильтрационная Гидроизоляция) или материала сооружений от вредного воздействия омывающей или фильтрующей воды или др. агрессивной жидкости (антикоррозийная Гидроизоляция). Работы по устройству Гидроизоляция называются гидроизоляционными работами. Гидроизоляция обеспечивает нормальную эксплуатацию зданий, сооружений и оборудования, повышает их надёжность и долговечность. Наша компания специализируется не только на инъекционной гидроизоляции, которая выполняется с помощью инъекционных пакеров, но и на других современных методах выполнения подобных работ.
