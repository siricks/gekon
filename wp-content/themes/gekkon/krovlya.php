<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/polimernaya-krovlya-chto-eto-takoe/">
                    <img src="/wp-content/uploads/2015/05/polimer-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/polimernaya-krovlya-chto-eto-takoe/">Полимерная кровля</a></div>
        </td>
        <td>
            <div><a href="/polimochevina-chto-eto-takoe-i-zachem-ona-nuzhna/">
                    <img src="/wp-content/uploads/2015/05/napyl-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="/polimochevina-chto-eto-takoe-i-zachem-ona-nuzhna/">Напыляемая на основе
                    полимочевины </a></div>
        </td>
    </tr>
    </tbody>
</table>
<div class="title_color titlePol">Полимерные кровли по степени нагрузки:</div>
<table class="tablePol">
    <tbody>
    <tr>
        <td>
            <div><a href="/wp-content/uploads/2015/05/exp.jpg">
                    <img src="/wp-content/uploads/2015/05/exp-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Эксплуатируемые</a></div>
        </td>
        <td>
            <div><a href="/wp-content/uploads/2015/05/neyexp.jpg">
                    <img src="/wp-content/uploads/2015/05/neyexp-150x150.jpg" alt=""/>
                </a></div>
            <div class="linkPol"><a href="#">Неэксплуатируемые</a></div>
        </td>
    </tr>
    </tbody>
</table>
<div class="div-page-menu-new">
    <ul id="pagetabs" class="tabs-22">
        <li><a href="#" class="page-button-new">ОПИСАНИЕ</a></li>
        <li><a href="#" class="page-button-new">ПРЕИМУЩЕСТВА ПОЛИМЕРНЫХ КРОВЕЛЬ:</a></li>
        <li><a href="#" class="page-button-new">ЦЕНЫ НА БЕСШОВНЫЕ КРОВЛИ:</a></li>
        <li><a href="#" class="page-button-new">МАТЕРИАЛЫ</a></li>
    </ul>
</div>
<div id="pagepanes" class="panes" style="">
    <div class="desc-desc">
        <div class="title_color titlePol">Коротко о полимерных кровлях</div>
        Фундаментом все начинается, а кровлей все заканчивается именно в этих двух узлах строения и необходимо делать
        самую качественную гидроизоляцию. На данный момент существует огромное количество кровельных материалов, у
        каждого из них есть свои плюсы и минусы. Но у всех кровельных систем кроме полимерных есть один большой минус,
        они все имеют швы. А где тонко, там и рвется. Шов это самое слабое место у любой кровли. Полимерные кровли
        позволяют избежать этот недостаток.

        Еще одним несомненным преимуществом является отличная адгезия, к любым материалам. Это позволяет наносить
        полимеры на любой тип старой кровли.
        <div class="title_color titlePol">Полимерная кровля</div>
        Состоит из полиуретановой мастики, желательно армированной стеклотканью. Полиуретановая мастика наноситься в два
        или три слоя валиками. Преимуществом мастики перед полимочевиной является простота в нанесении и более низкая
        цена.

        Полимерная кровля является неэксплуатируемой, по ней можно ходить, но интенсивных нагрузок она не выдержит.
        Очень просто наноситься на старые рулонные битумные кровли, легко промазываются все швы и примыкания.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4">Технология устройства</div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
        <div class="title_color titlePol">Полимочевина</div>
        Это, по сути, идеальная гидроизоляция, у нее есть недостаток, достаточно высокая цена, но про проблемы о
        гидроизоляции вы забудете надолго.
        Полимочевина является двухкомпонентным материалом, наносящимся специальными установками методом напыления,
        соответственно также не образует никаких швов. Обладает идеальной адгезией ко всем типам материалов. Высокие
        механические свойства, позволяют быть полимочевине эксплуатируемой кровлей, не боящейся очень больших нагрузок.
        Время реакции материала составляет 15 секунд, через минуту кровля готова к эксплуатации.
        <div class="clearfix">
            <div class="col3">
                <div class="title_color title_color4">Технология устройства</div>
            </div>
            <div class="col3">
                <div class="title_color title_color2">Материалы</div>
            </div>
            <div class="col3">
                <div class="title_color title_color5">Цена</div>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="title_color titlePol">Преимущество полимерных кровель</div>
        &nbsp;
        <div class="clearfix">
            <div class="col_l">
                <div class="title_color title_color2">Полимерные кровли</div>
                <div class="col_pad">

                    - Надежность;
                    - Стойкость к воздействию окружающей среды;
                    - Хорошие декоративные свойства;
                    - Отличная адгезия ко всем строительным материалам;
                    - Высокая эластичность;
                    - Паропроницаемость;
                    - Длительный срок службы
                    - Бесшовность

                </div>
            </div>
            <div class="col_r">
                <div class="title_color title_color3">Полимочевина</div>
                <div class="col_pad">

                    - хорошая адгезия к поверхностям различной природы;
                    - прекрасная ударопрочность, износостойкость, устойчивость при низких температурах;
                    - быстрое отверждение позволяет наносить покрытия из полимочевины на наклонные и вертикальные
                    поверхности
                    без образования подтеков и наплывов;
                    - отсутствие швов
                    - покрытие можно окрашивать
                    - покрытие практически непроницаемо для газов.

                </div>
            </div>
        </div>

        <div class="title_color titlePol">Преимущество полимерных кровель</div>
        Рассчитать цену он-лайн Вам, а также подобрать оптимальное для Вас решение поможет наш калькулятор.
    </div>
    <div class="prices">
        <div class="title_color titlePol">Цены на бесшовные кровли рассчитаны исходя из подготовленного основания, не
            требующего
            выравнивания.
        </div>
        <table width="100%">
            <tbody>
            <tr>
                <td width="269"></td>
                <td width="94"><strong>до 100 кв.м.</strong></td>
                <td width="81"><strong>от 500 кв.м.</strong></td>
                <td width="88"><strong>от 1500 кв.м.</strong></td>
                <td width="84"><strong>от 3000 кв.м</strong></td>
            </tr>
            <tr>
                <td width="269"><strong>Полиуретановые мастики</strong></td>
                <td width="94"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">материалы на 1 кв.м.</td>
                <td width="94">860</td>
                <td width="81">800</td>
                <td width="88">730</td>
                <td width="84">660</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td width="94">1500</td>
                <td width="81">1390</td>
                <td width="88">1240</td>
                <td width="84">1050</td>
            </tr>
            <tr>
                <td width="269"><strong>Полимочевина</strong></td>
                <td width="94"></td>
                <td width="81"></td>
                <td width="88"></td>
                <td width="84"></td>
            </tr>
            <tr>
                <td width="269">материалы на 1 кв.м.</td>
                <td width="94">900</td>
                <td width="81">860</td>
                <td width="88">810</td>
                <td width="84">740</td>
            </tr>
            <tr>
                <td width="269">Цена 1 кв.м. под ключ</td>
                <td width="94">1300</td>
                <td width="81">1100</td>
                <td width="88">990</td>
                <td width="84">900</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="materials">
        <div class="title_color titlePol">Основные материалы</div>
        &nbsp;
        <table class="table_null" border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="220"><img src="/wp-content/uploads/2015/04/mc.png" alt="" width="200"/></td>
                <td valign="top">Геккон МС. Полиуретановое связующее для резиновой крошки.</td>
            </tr>
            <tr>
                <td width="220"><img src="/wp-content/uploads/2015/04/test.jpg" alt="" width="200"/></td>
                <td valign="top">Резиновая крошка разных фракций</td>
            </tr>
            <tr>
                <td width="220"><img src="/wp-content/uploads/2015/04/test.jpg" alt="" width="200"/></td>
                <td valign="top">Резиновая крошка разных фракций</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
[contact-form-7 id="2487" title="заказать обратный звонок"]
<script> jQuery(document).ready(function () {
        jQuery("ul.tabs-22").tabs("div.panes > div");
    });
</script>
<strong>Полимерные
    кровли</strong> – это кровли сделанные из материалов изготовленных на основе полимеров. Существует два основных полимерных материала это полиуретановая мастика и полимочевина. Преимущества полимерных кровель очевидны, это бесшовность, прочность, очень высокий срок эксплуатации.


