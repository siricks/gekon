<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

	<section id="content">
		<div class="sb_nav_home">
			<nav class="sb_nav"><?php wp_nav_menu( array('theme_location' => 'МенюЛево', 'depth' => '1', 'container' => '' )); ?></nav>
			<div class="left_zr sb_rasch">
				<div id="left_zr" class="modal_win">
					<span class="modal_close"></span>
					<div class="modal_win_t">ФОРМА ЗАКАЗА ОБРАТНОГО ЗВОНКА</div>
					<?php echo do_shortcode('[contact-form-7 id="4" title="Заказать звонок"]'); ?>
				</div>
			</div>
			<div class="widget">
				<div class="sb_rasch"><a href= "/shop/materialy/">Каталог материалов</a></div>
			</div>
		</div>
		<div class="home_opisan">
			<div class="home_opisan_t">Что такое наливной пол</div>
			<div class="home_opisan_e"><p align="justify">Сегодня наливные полы пользуются все большей популярностью. Если вы посмотрите это видео, наверняка, поймете, почему. Особая техника исполнения и мастерский подход к работе позволяют полам "Геккон" радовать вас десятками лет.  Удивительного в популярность и востребованности такого материала, на самом деле, ничего нет, ведь он обладает огромным количеством достоинств, которые вы сможете использовать и проверить на практике.</p></div>
		</div>
		<div class="home_vid video">
			<iframe width="460" height="276" src="https://www.youtube.com/embed/ExDtKaMfLy0?start=6" frameborder="0" allowfullscreen></iframe>
			<div class="bannerV"><a href="/shop/materialy/">Тестировать</a></div>
		</div>
		<div class="clear"></div>
		<div class="preimush">
			<div class="preimush_t">4 причины работать с нами</div>
			<div class="preimush_list preimush1">
				<div class="preimush_list_icon"><span></span></div>
				<div class="preimush_list_t"><noindex>Комплексное снабжение</noindex></div>
				<div class="preimush_list_e"><noindex>Клиентам не нужно тратить время на разных поставщиков, ведь мы обеспечиваем их всех необходимым в нашей сфере.</noindex></div>
			</div>
			<div class="preimush_list preimush2">
				<div class="preimush_list_icon"><span></span></div>
				<div class="preimush_list_t"><noindex>Широкий ассортимент</noindex></div>
				<div class="preimush_list_e"><noindex>Наша компания продает материалы, инструменты, а также выполняет работы по всем типам полимерных покрытий</noindex></div>
			</div>
			<div class="preimush_list preimush3">
				<div class="preimush_list_icon"><span></span></div>
				<div class="preimush_list_t"><noindex>Лучшие цены</noindex></div>
				<div class="preimush_list_e"><noindex>Мы уверены не только в качестве наших продуктов и работ, но и в том, что они должны быть доступны каждому.</noindex></div>
			</div>
			<div class="preimush_list preimush4">
				<div class="preimush_list_icon"><span></span></div>
				<div class="preimush_list_t"><noindex>Большой опыт</noindex></div>
				<div class="preimush_list_e"><noindex>Наш опят исчисляется не просто сотнями объектов разного масштаба, но и тысячами довольных клиентов</noindex></div>
			</div>
			<div class="clear"></div>
		</div>
		<section id="posts">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="post">
				<h1><?php $title = the_title( '', '', false ); if (get_field('h1')) {the_field('h1');} else {echo $title;} ?></h1>
				<div class="entry entry_pad">
					<?php the_content() ?>
					<div class="seo_text"><? the_field('seo_text');?></div>
					<div class="clear"></div>
				</div>
			</article>
		<?php endwhile; endif; ?>
		</section>
		<aside id="sidebar">
			<div class="title title2"><noindex>Новости</noindex></div>
			<?php $news = new WP_query(); $news->query('showposts=3&cat=4'); ?>
			<?php while ($news->have_posts()) : $news->the_post(); ?>
			<div class="home_news_list">
				<div class="home_news_list_t home_news_list_t2"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
				<div class="home_news_list_d"><noindex><?php the_time('d/m/Y') ?></noindex></div>
				<div class="clear"></div>
				<div class="home_news_list_e"><noindex><?php echo raft_announce($post->post_content, 200); ?></noindex></div>
			</div>
			<?php endwhile; ?>
			<div class="home_news_all"><a href="<?php echo get_category_link(4); ?>">смотреть все</a></div>
			<br />
			<div class="title"><noindex>Статьи</noindex></div>
			<?php $news = new WP_query(); $news->query('showposts=3&cat=1'); ?>
			<?php while ($news->have_posts()) : $news->the_post(); ?>
			<div class="home_news_list">
				<div class="home_news_list_t"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
				<div class="home_news_list_d"><noindex><?php the_time('d/m/Y') ?></noindex></div>
				<div class="clear"></div>
				<div class="home_news_list_e"><noindex><?php echo raft_announce($post->post_content, 200); ?></noindex></div>
			</div>
			<?php endwhile; ?>
			<div class="home_news_all"><a href="<?php echo get_category_link(1); ?>">смотреть все</a></div>
		</aside>
		<div class="clear"></div>
		<div class="brends">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend1.jpg" />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend2.jpg" />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend3.jpg" />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend4.jpg" />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend5.jpg" />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brend6.jpg" />
		</div>
	</section>

<?php get_footer(); ?>