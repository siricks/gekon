<?php 
function callback($buffer) {
	return (str_replace("<title> – Геккон</title>", "<title>Ошибка 404: Страница не найдена</title>", $buffer));
}

ob_start("callback");
?>
<?php get_header(); ?>
	<section id="content">
		<section id="posts">
		

			<div class="breadc"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></div>

			<article class="post">
			<div class="entry entry_pad">
				<h1>Страница не найдена (404 Not Found)</h1>
<p>К сожалению, запрашиваемая вами страница не найдена на нашем сайте.</p>
<p>Это могло произойти по одной из следующих причин:</p>
<ul>
	<li>вы ошиблись при наборе адреса страницы (URL);</li>
	<li>вы перешли по &laquo;битой&raquo; (неработающей, неправильной) ссылке;</li>
	<li>запрашиваемой страницы никогда не было на сайте или она была удалена / перемещена.</li>
</ul>
<p>Мы приносим свои извинения за доставленные неудобства и предлагаем следующие пути:</p>
<ul>
	<li>вернуться назад при помощи кнопки браузера &laquo;back&raquo;;</li>
	<li>проверить правильность написания адреса страницы (URL);</li>
	<li>перейти на главную страницу;</li>
</ul>
</div>
<div class="clear"></div>
			</article>

			<?php $images_bot = get_post_meta(get_the_ID(), 'ar_images_bot', true); ?>

		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
		<?php include (TEMPLATEPATH . '/primeri.php'); ?>
	</section>

<?php get_footer(); ?>